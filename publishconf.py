# This file is only used if you use `make publish` or
# explicitly specify it as your config file.

import os
import sys

sys.path.append(os.curdir)
from pelicanconf import *

# If your site is available via HTTPS, make sure SITEURL begins with https://
SITEURL = "https://ewen.corre.bio"
RELATIVE_URLS = False

FEED_ALL_ATOM = "feeds/all.atom.xml"
FEED_ALL_RSS = "feeds/all.xml"
CATEGORY_FEED_ATOM = "feeds/{slug}.atom.xml"
TRANSLATION_FEED_ATOM = "feeds/{lang}.atom.xml"


DELETE_OUTPUT_DIRECTORY = True

# Following items are often useful when publishing

# DISQUS_SITENAME = ""
# GOOGLE_ANALYTICS = ""

WEBRING_FEED_URLS = [
    "https://chagratt.site/index.xml",
    "https://blog.erreur503.xyz/feeds/all.rss.xml",
    "https://lamecarlate.net/carnet.rss",
    "https://blog.isabelle-santos.space/index.xml",
    "https://epyisageek.net/feeds/all.atom.xml",
    "https://alennebrezhoneg.wordpress.com/feed/",
    "https://www.jeey.net/feed/rss",
    "https://blog.chibi-nah.fr/feeds/all.atom.xml",
]
