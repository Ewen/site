---
title: Ur blog nevez
date: 2022-03-31
status: published
lang: br
slug: nouveau-blog
category: Perso
translation: true
tags: blog, brezhoneg
---

Salud d'an holl!

Setu, graet eo, savet em eus ur blog all.
Un nebeud blogoù all am eus graet, dindan ma anv gwir pe dindan lesanvioù.
Ha neuze, setu unan nevez. N'eo ket fall, geo?

## Piv on?

Ewen Korr on, ur paotr eus Breiz Izel.
Graet em eus ma studioù kentoc'h er bed skiantel, aet on kuit abalamour ne oan ket sur kavout ma flas din-me er bed-se.
Heuliet em eus ur stummadur hir war ar brezhoneg.
Ha bremañ emaon o labourat e Ofis publik ar brezhoneg, e servij an Arsellva, a heuil emdroadur an implij ar yezhoù e Breizh.

Estreget ar vicher zo evel-just: me zo sot gant ar podkastoù, gant ar meziantoù frank.
Plijout a ra din bezañ gouest d'ober an traoù ma-unan (gant sikour pe diwar labour ar re all, un tamm evel ar skeudenn "war divskoaz ar ramzed").
Redek a ran, mont a ran war velo pa c'hellan.

Evit gouzout muioc'h diwar ma fenn, setu _ar_ bajenn: [Piv-on?](/pages/a-propos.html).

## Petra vo kavet amañ?

Petra vo kavet ? Traoù a-bep seurt a zo dedennus din, ha marteze dedennus deoc'h ivez.

- traoù war an dielfennañ roadennoù (gant `R` dreist-holl)
- podkastoù (aliet ganin pe ivez fardet ganin)
- alioù lenn, alioù c'hoari

Klask a rin sevel ar pennadoù en 2 yezh, brezhoneg ha galleg.
N'eo ket ar pennad kentañ, rak enporzhiet em eus pennadoù skrivet a-raok, evit ma ne vefe ket a heklev amañ :-)

Daou dra zo da notenniñ:

- ur blog personel eo, ma c'homzoù eo nemetken
- pep tra zo embannet dindan aotre [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.br). Ar pezh a sinifi e c'hell pep hini kemer ma skridoù ha ma labour hag ober kement tra en eus pe he deus c'hoant. Evit gounit arc'hant zoken. Daou dra hepken zo d'ober: lakaat ma anv e-kichen ar pezh produet (ar `BY`) ha rannañ al labour graet dindan aotre Creative Commons ivez (`SA`, evit _share alike_, rannañ memes-mod).

## Penaos mont e darempred ganin?

- dre ar fediverse: [@ewen@mastodon.fedi.bzh](https://mastodon.fedi.bzh/@Ewen)
- dre bostel: blog [e ti] korr.bzh

Setu prest omp, deomp dezhi!

Ken 'vioc'h lennet! 👋
