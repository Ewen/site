---
title: Nouveau blog
date: 2022-03-31
status: published
lang: fr
slug: nouveau-blog
category: Perso
translation: false
tags: blog
---

Salut tout le monde !

Ayé, c'est fait, voilà un (nouveau) blog.
J'ai déjà monté des blogs par le passé, sous pseudonyme ou bien sous ma propre identité.
En voilà un autre, un poil plus professionnel.

## Qui suis-je ?

Je m'appelle Ewen Corre, je viens de basse Bretagne.
J'ai fait mes études principalement dans le monde scientifique, que j'ai quitté par la suite principalement par manque de place.
Ensuite j'ai suivi une formation longue en breton, 9 mois à temps plein.
Et je travaille actuellement à l'_Ofis publik ar brezhoneg_, au sein du service de l'Observatoire, qui étudie l'évolution de l'utilisation des langues en Bretagne.

Il n'y a pas que le boulot : j'aime beaucoup les podcasts (ou balados, j'utilise les deux termes) et le monde du logiciel libre.
J'aime bien être capable de faire les choses moi-même, en autodidacte. Évidemment cela se fait avec l'aide d'autres personnes, ou au moins à partir du travail des autres, un peu comme l'image "sur les épaules des géants".
Sinon, je fais un peu de course à pied et j'utilise mon vélo quand je peux.

Pour plus d'infos, voilà _la_ page : [À propos](/pages/a-propos).

## Contenu du site

Que va-t-on trouver sur ce site ? A priori, des choses qui n'ont pas forcément grand-chose à voire entre elles, si ce n'est qu'elles m'intéressent. Et qu'elles vous intéressent, peut-être.

- des articles sur l'analyse de données (avec `R` surtout)
- des balados (des conseils d'écoute ou bien des balados que je réalise)
- des avis de lecture, de jeux vidéo

J'essaierai d'écrire les articles, et plus globalement le contenu du site, en breton et en français.
Si cet article n'est pas le premier, c'est parce que j'ai importé un peu de contenu écrit par le passé, histoire qu'il n'y ai pas trop d'écho ici. :-)

Deux choses sont à noter :

- c'est un blog personnel dont le contenu n'engage que moi
- tout le site (sauf mention contraire) est publié sous licence [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.fr). Cela signifique que tout le monde peut utiliser mes écrits ou mon travail et en faire ce qu'iel veut. Même pour se faire de l'argent. Il y a deux choses à faire tout de même : me donner crédit, en indiquant mon nom à côté de la chose produite à partir de mon travail (c'est la clause `BY`), et partager l'œuvre réalisée sous une licence Creative Commons également (clause `SA`, pour _share alike_, partage à l'identique).

## Comment me joindre ?

- sur le fédivers : [@ewen@mastodon.fedi.bzh](https://mastodon.fedi.bzh/@Ewen)
- par courriel : blog [e ti] korr.bzh

Allez, tout est prêt,

Ken 'vioc'h lennet!
