---
title: BreakTimer et mal de dos
date: 2025-01-14
category: Perso
tags: santé, posture, outil
---

Parlons un peu tech, un peu boulot et un peu santé.

## TL;DR
J'ai régulièrement mal au dos, maintenant lorsque je assis à un bureau j'essaie de faire des pauses régulières, et pour ça j'utilise [BreakTimer](https://breaktimer.app/).

---

## Travail, posture, sédentarité

Il y a quelques mois, après quelques analyses[ref]un rendez-vous chez ma généraliste, un examen posturologique chez un podologue, puis une IRM (avec ChérieFM dans le casque 😶).[/ref], un diagnostic a été posé sur une gêne que j'avais au dos depuis plusieurs mois et même en fait plusieurs années : **hernie discale**.

De ce que j'ai compris, l'association hernie - douleur n'est pas systématique. La proportion de gens avec hernie est la même, que ce soit des personnes souffrant de douleurs au dos ou non[ref]Bon là évidemment je ne trouve plus de référence, et en même temps je n'en avais pas lu moi-même dans des revues scientifiques à comité de lecture, donc c'est une information à prendre avec des pincettes.[/ref].

Quoi qu'il en soit, une bonne partie de l'année 2024 a été douloureuse.

Je me suis posé la question de l'origine de ce mal de dos.
Est-ce que je l'ai abîmé en faisant de la [batucada](https://youtu.be/omZz25nrbtw?feature=shared&t=324)[ref]Non, je n'ai pas joué dans une batacuda aussi impressionnante qu'Aainjaa, mais elle vaut le détour ![/ref] ? 
En bricolant et portant des charges lourdes, même rarement ?
À cause de la course à pied sur route ?
À cause de la position assise prolongée, depuis un temps qui se compte maintenant en années ?

Difficile à dire.
Je me dis que c'est sûrement un mélange de tout ça.
En tout cas, maintenant que ça fait quelques mois que je vis avec, j'ai remarqué que **plus je bouge, moins j'ai mal**.
C'est je pense un conseil qu'à peu près toustes les pros de la santé disent.

Et comment qu'on fait quand on travaille devant un ordinateur toute la journée, a fortiori en télétravail, de chez soi ?
Je ne prends pas le métro ni la voiture pour aller aux toilettes ou me servir un café, mais mes déplacements sont quand même limités…

Alors je me suis dit qu'il fallait que je bouge pendant ma journée de travail.
Je ne suis pas la première personne à se faire cette réflexion, [un certain Luc fait du vélo à son bureau](https://fiat-tux.fr/2024/03/21/me-voila-velotafeur-sans-bouger-de-chez-moi/) par exemple.
Je me suis orienté vers une autre solution, de mon côté.

## Kiné et pilates
Les séances de kiné que j'ai pu faire ont petit à petit commencé à faire effet.
Ma kiné m'avait proposé des exercices de **renforcement musculaire**, pour muscler ma ceinture abdominale : gainage, étirements, tout ça.
Elle m'a par ailleurs suggéré d'utiliser une application qui propose des séances d'à peu près tout ce qu'on peut faire dans son salon (pilates, yoga, étirements, musculation…) : [FitOn](https://fitonapp.com).
Il y a là-dessus vraiment beaucoup de contenu, c'est difficile de s'ennuyer.
Le risque est plutôt de faire trop au début, et de ne pas tenir le rythme sur le long terme.

Et c'est ce qui m'est arrivé d'ailleurs, en décembre j'ai un peu laissé tombé. Ça et l'inactivité liée aux repas et transports pour les fêtes de fin d'année ne m'ont pas épargné : **j'ai commencé 2025 en ayant plus mal que d'habitude**.

Fort de mes bonnes résolutions, je m'y remets, mais, en fait, ce n'est pas grave si ce n'est pas pérenne : ce que m'apporte l'appli c'est **la connaissance de mouvements que je peux faire par la suite**.
Et on arrive, enfin, au petit outil technique que j'utilise.

## BreakTimer
[BreakTimer](https://breaktimer.app/) est un petit logiciel qu'on peut installer sur son ordinateur, et qui propose (ou oblige, c'est selon) de **faire des pauses**. Par exemple, une pause de 2 minutes toutes les 30 minutes.

[![Capture de BreakTimer]({static}/media/2025/breaktimer.png){: .image-process-large-image }]({static}/media/2025/breaktimer.png)
/// caption
Capture d'écran de BreakTimer. *Un ehan bremañ !* signifie en breton : *Une pause maintenant !*
///

Et c'est donc ce que je fais : des pauses courtes, mais régulières, pour **éloigner mes yeux de l'écran** et les reposer.
Je mets à profit ces 2 ou 3 petites minutes pour **m'étirer, pour faire les mouvements** que j'avais identifié comme me faisant du bien.

En fin de journée, je sais tout de suite si j'ai fait des pauses comme ceci ou non : moins de fatigue occulaire, moins de raideur dans le dos.
C'est vraiment agréable.

En pratique, je peux avoir du mal à m'arrêter à chaque fois, c'est difficile de s'arrêter complètement de bosser quand on est bien concentré sur un truc.
Mais j'ai réussi à ne pas occulter purement et simplement la notification quand elle apparaît sur mon écran, et je pense que je respecte une majorité de pauses.

Je recommande donc ce logiciel, que vous ayez mal au dos ou non : dans tous les cas c'est bon de changer de position et de se reposer les yeux !

Oh, c'est l'heure de la pause d'ailleurs ! 🤸
