---
title: « J'apprends le breton ! » : un nouveau balado
date: 2020-09-17
status: published
category: Perso
lang: fr
tags: podcast, podkast, brezhoneg
---

Pourquoi apprendre le breton, une langue supposée « morte » par certain·e·s ?
Eh bien justement pour prouver le contraire.
Plus sérieusement, c'est pour ajouter une brique manquante à mon identité bretonne.
Et aussi pour m'ouvrir des portes professionnellement, comme dans l'enseignement en breton.

Et, pour être encore moins oisif, j'ai voulu mettre en boîte cette petite aventure, qui durera au moins 6 mois.
J'en fais donc un balado (ou _podcast_ pour les anglophones) !

Voilà où écouter les épisodes, qui vont arriver petit à petit : [https://podkast.fedi.bzh/@japprends_le_breton](https://podkast.fedi.bzh/@japprends_le_breton).
Si vous préférez le flux RSS à mettre dans votre agrégateur de flux ou votre lecteur de balados c'est ici : [flux RSS](https://podkast.fedi.bzh/@japprends_le_breton/feed.xml).

Je pense publier un épisode à peu près toute les semaines.
J'en profiterai pour évoquer ce qu'on a appris, les méthodes pédagogiques, des petites anecdotes.
Et puis j'essayerai de parler un peu breton, histoire de voir l'évolution des premiers épisodes aux derniers, c'est un peu l'objectif de la démarche. ☺

Un autre objectif, c'est de m'auto-former à la production audio : savoir utiliser un micro, à la fois pour parler dedans et monter le tout.
À l'heure où j'écris ces lignes, j'ai déjà publié le premier épisode et je vois pas mal de points d'amélioration : le rythme n'est pas très agréable, je fais de nombreux « euh… » et je me répète. Je vais tâcher de m'améliorer. Enfin écoutez l'épisode quand même hein ! ^^'

Après près de deux semaines de formation, je suis hyper motivé par la langue bretonne, et ai envie de l'utiliser au quotidien. Du moins, dès que ce sera possible, d'ici quelques semaines.
Alors qui sait, peut-être qu'il y aura une section en breton sur ce blog, pour remplacer la section anglaise, totalement inutile ?

_Ken ar c'hentañ!_

**EDIT** : Et voilà ! :D
