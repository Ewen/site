---
title: Le thème sombre arrive sur le blog ! 
date: 2025-02-01
category: Perso
tags: blog, css
---

Bonjour, bonsoir !

Court article de blog pour annoncer l'arrivée du thème sombre sur le blog. 🌘

Le choix des couleurs est aussi douteux que le thème clair, et j'en suis désolé. Mes compétences graphiques et artistiques ont des limites. 🤷

Je me suis inspiré de l'ami tenancier du blog [Erreur503](https://blog.erreur503.xyz/) en regardant son code CSS.

Et alors, comment ça fonctionne ?
Il a d'abord mettre toutes les mentions à des couleurs dans des variables CSS, dans un bloc `:root`, comme ceci :

```css
:root {
  --background-body: #f5f5ef;
  --text-color: #343427;
  --background: #ffffff;
  /* … */
}
```

Dans le reste des blocs CSS, on peut les utiliser ainsi :

```css
body {
  background-color: var(--background-body);
  color: var(--text-color);
  /* … */
}
```

Et pour que la magie opère, on va écraser la valeur des variables si l'utilisateur est en mode sombre, comme ça :

```css
@media (prefers-color-scheme: dark) {
  :root{
    --background-body: #232322;
    --text-color: #fefef8;
    --background: #454642;
    /* … */
    }
}
```

Tadaaa 🎉

Le commit apportant ce changement peut être retrouvé [dans mon dépôt sur Codeberg](https://codeberg.org/Ewen/site/commit/a7e60f564570cebb68e68e47ef89be8b6240128b).
