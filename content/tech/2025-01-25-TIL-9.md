---
title: TIL #9 - Permalien de Neovim à Git(hub), message de commit et ancre HTML avec n'importe quel bout de texte
date: 2025-01-25
category: Tech
tags: TodayILearned, git, neovim, reflog, commit, cpu, tests, html, url
---

## Neovim et git : récupérer le permalien d'une ligne de code
Lorsqu'on édite un fichier versionné avec Git, on peut utiliser une chouette commande qui donne le lien de la ligne et du fichier sur la forge logicielle choisie.

Pour ça, il faut avoir installé et configuré sur Neovim [vim-fugitive](https://github.com/tpope/vim-fugitive) (et une dépendance, [vim-rhubarb](https://github.com/tpope/vim-rhubarb)).

On sélectionne la ligne ou le bloc de code, et on utilise la commande suivante :
```
:'<,'>GBrowse! master:%
```

Ici, on commence par `:'<,'>` qui est automatiquement rempli quand on tape `:` en mode visuel.
La commande c'est donc `GBrowse` ou `GBr` (toutes les commandes de `vim-fugitive` commencent par `G`).
`!` permet de ne *pas* ouvrir le navigateur directement, ça met le lien dans le presse-papier.
Enfin, `master:%` permet de sélectionner la branche `master`, plutôt que d'utiliser la branche courante, qui n'existe peut-être pas sur la forge logicielle.


## Git : retrouver le message d'un commit après l'avoir réinitialisé (`git reset`)
Ce matin, j'ai voulu retravailler le dernier commit sur une branche, et j'ai donc fait
```bash
git reset --soft HEAD~1
git restore --staged .
```

J'avais fait exprès de garder une fenêtre de terminal ouverte avec le message du commit avant de faire ça.
Et par réflexe de propreté de mon environnement, je l'ai fermée ! Donc, perdu le message.

Heureusement, `git reflog` est là, et c'est l'occasion pour moi de l'utiliser pour de vrai.

```bash
git reflog 
# Je repère la ligne qui mentionne mon commit, ici HEAD@{2}
git log HEAD@{2} # et voilà !
```

Maintenant je n'aurai plus besoin de garder une fenêtre ouverte. 


## Git : ajouter un commit d'une autre branche dans la branche courante
Ça se fait avec `git cherry-pick` !

```bash
git cherry-pick <commit>
```

Et `git rebase -i master` pour réorganiser les commits, si besoin.


Mais en vrai, un `rebase` devrait suffire :

```bash
git rebase -i mon-autre-branche
```


## CPU
Au travail, je lance des tests automatiques pour voir si ce que je trifouille dans le codec asse quleque chose ou non.
Il y a plus de 3600 tests, et ça prend environ 3 minutes. 
J'ai voulu essayer d'accélerer la suite. J'imagine qu'il y a plusieurs solutions. Là, j'ai testé de changer la gouvernance du CPU (*scaling governor* en anglais).

J'ai remarqué avec `cpupower frequency-info` que je suis en mode `powersave`, donc les CPUs sont limités pour ne pas trop utiliser d'énergie. Or, je suis sur secteur, donc ça ne devrait pas être limitant !

Hop, un coup de `cpupower frequency-set -g performance`, ça améliore un peu le truc.

Source : [Archlinux wiki - CPU frequency scaling](https://wiki.archlinux.org/title/CPU_frequency_scaling#Scaling_governors)


## Utiliser un bout de texte dans une page web pour y atterrir avec un lien
Cette astuce vient tout droit de [Joachim](https://blog.professeurjoachim.com/billet/2024-12-03-lier-un-fragment-de-texte-dans-une-page-web), et des copaines fédéré·e·s ont relayé ça jusqu'à mes yeux ébahis.

À une URL, on peut ajouter une sorte de suffixe, ça : 
```
#:~:text=Ceci est du texte
```

Par exemple, on peut écrire ce lien : [https://ewen.corre.bio/breaktimer-et-mal-de-dos.html`#:~:text=Par exemple%2C une pause de 2 minutes`](/breaktimer-et-mal-de-dos.html#:~:text=Par exemple%2C une pause de 2 minutes), et le navigateur va aller à la page et surligner la première occurrence de ce qui est cherché.

Il faut un navigateur assez récent, mais d'ici quelques mois/années, a priori tout le monde pourra utiliser ce standard ([page sur MDN](https://developer.mozilla.org/en-US/docs/Web/URI/Fragment/Text_fragments)) ! 🤯
