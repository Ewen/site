---
title: Sevel kartennoù gant R hag OpenStreetMap
date: 2021-04-05
lang: br
category: Tech
tags: brezhoneg, kartennoù, Rstats, OpenStreetMap, tuto
---

<div class="info">
Graet em eus ur staj e Ofis Publik ar Brezhoneg evit dizoleiñ un tamm muioc'h penaos eo bed ar Brezhoneg. Komzet em eus gant tud simpa kenañ eno ha fardet em eus kartennoù Breizh !
Setu ur seurt tuto hag a zispleg tamm-ha-tamm penaos em eus graet.
</div>

## Kendegouezh

Lakaomp e fell deoc'h diskouez un dra e-keñver Breizh.
Da skouer keitad priz ar metrad karrez dre gumun e Breizh a-bezh.
Gallout a rafec'h dastum ar roadennoù ha sevel ur gartenn gant an dorn.
Met evel-just e vefe hir spontus.
Ha p'ho po c'hoant da adsevel ar gartenn ar bloaz war-lerc'h e tleoc'h adober pep tra.

Un doare all d'ober eo implijout ur meziant a c'hell bezañ implijet evit al labour a seurt-se.
Da skouer gant [QGIS](https://qgis.org) a zo ur meziant evit ober kartennoù. Ha [setu un tutorial](https://www.tuto-carto.fr/qgis-creer-une-carte/).

Met kavout a ran gwelloc'h un doare all c'hoazh : implijout `R` a zo ur yezh stlennegel evit ober stadegoù ha dielfennadurioù.
Peogwir eo ur yezh stlennegel eo kalz aesoc'h aotomatekaat/emgefrekaat traoù.
Neuze er pennad-mañ emaon o vont da ginnig penaos ober kartennoù gant `R`.

## Petra hon eus ezhomm ?

### Ar meziantoù

Ezhomm hon eus `R`, ha netra ken.
R a zo ur yezh stlennegel evel em eus lavaret.
Ar pezh aesañ eo pellgargañ ha staliañ [`RStudio`](https://www.rstudio.com/products/rstudio/#rstudio-desktop) a zo un endro evit programmiñ fardet a-ratozh evit ar yezh R.
A-drugarez d'ar meziant-se e c'heller skrivañ urzhioù, gwelout disorc'hoù ha me oar me.
Neuze kit da bellgargañ `RStudio` ha staliit anezhañ.

Setu penaos eo aozet an etrefas :
![Etrefas `RStudio`]({static}/media/2021/rstudio.png)
**Etrefas `RStudio`**

Un displegadennig :

1. lec'h ma vo skrivet ar skript
2. lec'h ma c'heller skrivañ urzhioù ivez, da skouer evit amprouiñ traoù a-raok skrivañ anezho er skirpt
3. lec'h ma vo kavet ar variennoù (gwelet e vo diwezhatoc'h)
4. lec'h ma vo diskouezet an disorc'hoù, ar gartenn da skouer !

### Ar roadennoù

Ezhomm hon eus 2 dra e-keñver ar roadennoù :

1. ar roadennoù douaroniel : evit tresañ ar gartenn
2. ar roadennoù da ziskouezañ. En hor skouer : priz ar metrad karrez e Breizh

Evit ar roadennoù douaroniel emaomp o vont da implijout roadennoù [OpenStreetMap](https://openstreetmap.org) (OSM).
Chañs hon eus rak ul levraoueg, da lavaret eo ur seurt lugant (_plug-in_), a zo evit adtapout roadennoù OSM dre `R` ! 🎉

Hag evit ar roadennoù da ziskouezañ eo simpl a-walc'h : kavet em eus roadennoù evit Frañs a-bezh war [data.gouv.fr](https://data.gouv.fr).
Setu ul liamm evit pellgargañ anezho : [_Prix moyen au m² des ventes de maisons et d'appartements par commune en 2019_](https://www.data.gouv.fr/fr/datasets/prix-moyen-au-m2-des-ventes-de-maisons-et-dappartements-par-commune-en-2019/).

Mat eo ! Bremañ p'hon eus pep tra e c'hellomp kregiñ e-barzh ! 🤸‍

## Krogomp !

### Staliañ al levraouegoù

Da gentañ ma digarezit rak livet em eus gevier.
Ret eo deomp staliañ traoù all, met e-barzh R ar wech-mañ (pe `RStudio` evit bezañ resis).
Ezhomm hor bo al levraouegoù-se :

- `ggplot2` : evit tresañ grafikoù, kartennoù
- `osmdata` : evit pellgargañ roadennoù OSM
- `dplyr` : al levraoueg-mañ a sikouro skrivañ urzhioù poellekoc'h ha buanoc'h
- `sf` : evit dazverañ roadennoù OSM

Neuze lañsit `RStudio`.
Krouit ur restr nevez (da skouer e saozneg : _File > New File > R script_) ha saveteit anezhi en un teuliad bennak.
Da skouer en teuliad e lec'h m'ho peus pellgarget ar roadennoù.

Bremañ e-barzh ar c'hoñsol (al letrin) skrivit :

```r
install.packages(c("ggplot2", "osmdata", "dplyr", "sf"))
```

Goude ur pennad amzer e vo al levraouegoù-se staliet war ho urzhiataer.

### Aozañ ar skript

Bremañ aozomp hor skript evit gouzout petra ober dre vras.

Ret eo deoc'h gouzout un tamm petra eo ereadurezh `R`.
Ma fell deoc'h e kinnigan deoc'h mont da welout an [tuto-se](https://juba.github.io/tidyverse/index.html) (e galleg).
N'eo ket ur redi newazh.

Rankout a ra deoc'h gouzout petra eo an evezhiadennoù memestra.
Int a zo un doare da skrivañ traoù evidoc'h hepken, ne servijont da netra nemet aozañ ar skript ha reiñ da gompren petra a dalv urzhioù luziet da skouer.
E `R` e vezont skrivet gant an arouezenn `#`. Ur skouer :

```r
# Setu un evezhiadenn. Al linenn dindan a jed ar gwezhiadur 2+2

2+2
```

Neuze implijet e vo an evezhiadennoù evit aozañ ar skript dre vras :

```r
##########

# 1. Kargañ al levraouegoù

##########

##########

# 2. Dastum roadennoù OpenStreetMap

##########

##########

# 3. Dastum roadennoù ar prizioù

##########

##########

# 4. Eren (joindre, merge) ar roadennoù

##########

##########

# 5. Tresañ an disoc'h !

##########
```

Gwelout a rit ez eus 5 lodenn. Krogomp gant an hini gentañ !

### 1. Kargañ al levraouegoù

Al lodenn-mañ a zo an hini simplañ.
Staliet hon eus al levraouegoù, bremañ eo poent o c'hargañ gant an urzh : `library()`.
Er mod-se e vez graet :

```r
##########

# 1. Kargañ al levraouegoù

##########
library("ggplot2")
library("osmdata")
library("dplyr")

# Evit lavarout da R e peseurt teuliad emaomp o labourat

setwd("~/Documents/Brezhoneg/stajou/2021-03_ofis/kartenn/")
```

Gallout a rit diuzañ an teir linenn a grog gant `library` ha klikañ war "Run" en nec'h, a-zehou, pe pouezañ war `Ctrl + stokell Kas`.
An urzhioù a vo kaset ha karget e vo al levraouegoù !

Ouzhpennet em eus un urzh all evit lavarout da `R` e peseurt teuliad emaomp o labourat (gant `setwd()` a dalv _set working directory_ e saozneg).

Kendalc'homp gant un dra un tamm diaesoc'h : kargañ roadennoù OpenStreetMap.

## 2. Dastum roadennoù OpenStreetMap

### a. Pellgargañ

Al levraoueg `osmdata` a bourchas un urzh anvet `opq()` evit kas ur reked, ur goulenn da OpenStreetMap.

```r
##########

# 2. Dastum roadennoù OpenStreetMap

##########
roadennou_osm <- opq(bbox=c(-5.5584,46.7773,-0.7409,48.9728),
timeout = 180) %>%
add_osm_feature(key = 'boundary', value = 'administrative') %>%
add_osm_feature(key = 'admin_level', value = '8') %>%
osmdata_sf()
```

Ar pezh pouezusañ en urzh-mañ eo an arventenn `bbox` a sinifi _bounding box_ (e brezhoneg : "_boest hazh_").
Lakaet e vo enni daveennoù douaroniel Breizh, skrivet er mod-se : `c(-5.5584,46.7773,-0.7409,48.9728)`.
`c()` a zo un urzh evit bodañ talvoudoù en ur sturiadell.

Lakaat a ran ivez `timeout = 180` rak pounner eo ar roadennoù da bellgargañ.
Dre ziouer ma n'eo ket echu ar bellgargañ dindan 25 eilenn ez eus ur fazi _timeout_.
Ret eo hiraat an talvoud. Gallout a rit klask gant 180 eilenn da gentañ hag uhelaat ma n'eo ket trawalc'h.

Goude e implijan div wech ar memes urzh : `add_osm_feature()`.
Resisaet eo ar reked evit kaout ar c'humunioù ([live melestradurel](https://wiki.openstreetmap.org/wiki/FR:Key:admin_level) 8 a zo ar c'humunioù).

Erfin e ouzhpennan `osm_data_sf()` evit kaout ar roadennoù en ur furmad a zo aes da zazverañ.

### b. Mirout ar roadennoù talvoudus hepken

N'eo ket echu c'hoazh !
Kalz a ditouroù a zo didalvoud neuze eo gwelloc'h silat un tamm.

```r
kumuniou <- roadennou_osm$osm_multipolygons %>%
mutate(departamant = substr(ref.INSEE, 1,2)) %>%
filter(departamant %in% c("22", "29", "35", "44", "56"))
```

El lodenn-mañ e implijañ ereadurezh `dplyr` a zo simpl a-walc'h evit kompren petra zo graet amañ.

Da gentañ e krouan ur varienn nevez, `kumuniou`. Lakaet vo enni ar pezh a zo jedet a-zehou.

Meur a roadenn zo e `roadennou_osm`.
Dibab a ran ar pezh anvet `osm_multipolygons` hepken peogwir ez eus ennañ stumm, anvioù ar c'humunioù ha titouroù all.

An eil linenn a grog gant `mutate()`.
Ar fonksion-se (an urzh-se) a laka ur golonenn nevez en daolenn `osm_multipolygons`.
Kemer a ran ar varienn `ref.INSEE`, arloañ a ran warni ar fonksion `substr()` evit mirout an daou niverenn gentañ er c'hod INSEE.
An disoc'h eo niverenn an departamant ha lakaet eo er golonenn nevez.

El linenn ziwezhañ e silan an disoc'hoù evit mirout departamantoù Breizh hepken.

C'hoant ho peus sellout ouzh ar pezh hon eus evit poent ?
Aes eo !

```r
ggplot(kumuniou, aes(fill=departamant))+
geom_sf()
```

Implijout a ran `ggplot()` evit krouiñ ur grafik nevez.
Reiñ a ran dezhi ar roadennoù (`kumuniou`).
Goude e lavaran dezhi lakaat ul liv disheñvel evit pep departamant (`aes(fill=departamant)`).
Skrivet eo war-lerc'h an urzh a zo gouest da sevel ur gartenn diwar ar roadennoù (`geom_sf()`).

Ha setu an disoc'h !

{{< figure src="./kartenn_breizh_departamant.png" caption="An disoc'h evit poent : kartenn Breizh gant an holl gumunioù livet dre zepartamant." class="center img-90" alt="An disoc'h evit poent : kartenn Breizh gant an holl gumunioù livet dre zepartamant.">}}

Brav ! 👏

## 3. Dastum roadennoù ar prizioù

Livañ ar c'humunioù dre zepartamant a zo mat met n'eo ket ar pezh a oa raktreset.
Evit livañ ar c'humunioù dre briz ar metrad karrez eo ret kargañ ar roadennoù-mañ.
Goude bezañ pellgarget ar restr e c'hellit skrivañ :

```r
##########

# 3. Dastum roadennoù ar prizioù

##########
priziou <- read.csv("./dvf-communes-2019.csv", sep=";") %>%
as_tibble %>%
filter(INSEE_DEP %in% c("22", "29", "35", "44", "56"))
```

Amañ `read.csv()` a zo un urzh evit kargañ un teul CSV (_Comma Separated Values_).
Talvoudoù an teul am eus dibabet a zo dispartiet gant an arouezenn `;` setu perak em eus skrivet `sep=";"`.

Amdreiñ a ran ar roadennoù e furmad `dplyr` a-drugarez da `as_tibble` evit bezañ gouest da zazverañ aes.
Hag erfin memes tra eget a-raok : silat a ran ar roadennoù evit mirout kumunioù e departamantoù Breizh hepken.

Prest omp bremañ evit eren/liammañ an holl ditouroù !

## 4. Eren ar roadennoù

Emaomp o vont da implijout an titour a zo boutin d'an daou daolenn : an niverenn INSEE.
Setu penaos ober :

```r
##########

# 4. Eren (joindre, merge) ar roadennoù

##########
data <- merge(kumuniou,
priziou,
by.x="ref.INSEE",
by.y="INSEE_COM", all.x=TRUE)
```

Ar fonksion `merge()` a gemer ar roadennoù `kumuniou` hag ar re `priziou`.
Lavarout a reomp implijout an niverennoù INSEE evit keñveriañ ha liammañ an titouroù.
Da lavaret eo ar golonenn `ref.INSEE` en daolenn gentañ hag ar golonenn `INSEE_COM` en eil daolenn.

Bremañ ez eus kalz a ditouroù e-barzh ar varienn `data`.
Un tammig re evit lavarout ar wirionez.
Neuze e simplaan/reduan anezhi er mod-se :

```r
data_simpl <- data %>%
select(ref.INSEE, name, name.br, geometry,
POPULATION, Nb_Ventes, PrixMoyen_M2) %>%
st_simplify(preserveTopology = TRUE, dTolerance = 0.001)
```

An urzh-mañ a lavar :

1. kemer ar varienn `data`
2. dibab meur a golonenn (`ref.INSEE`, `name`, `name.br` hag all hag all)
3. implij ar fonksion `st_simplify()` evit reduiñ resisted stumm ar c'humunioù rak pounner a-walc'h eo
4. laka an disoc'h en ur varienn nevez anvet `data_simpl`

Dastumet hag aozet hon eus kement tra hon eus ezhomm evit tresañ an disoc'h ! 👌

## 5. Tresañ an disoc'h

Evit ar bazenn ziwezhañ emaomp o vont da implijout `ggplot()` evel ar gartenn gentañ.
Setu ar pezh am eus skrivet :

```r
##########

# 5. Tresañ an disoc'h !

##########
ggplot(data_simpl, aes(fill=PrixMoyen_M2))+
geom_sf()+
scale_fill_gradientn(
colors = c("#ffdbdb", "#ea0000"),
space = "Lab",
na.value = "grey75",
guide = "colourbar",
aesthetics = "fill",
name="Priz"
)+
ggtitle("Keitad priz ar metrad karrez dre gumun e Breizh")+
theme_void()+
theme(plot.title = element_text(face = "bold", hjust=0.5, size=15))
```

Memes tra eget a-raok : reiñ a ran da `ggplot()` ar roadennoù, da lavaret eo ar varienn `data_simpl`.
Goude e lec'h an departamantoù e lavaran dezhi implijout ar golonenn `PrixMoyen_M2` a oa en daolenn `priziou` ha neuze bremañ en daolenn `data_simpl`.
Lavarout a ran ivez implijout `geom_sf()` evit tresañ.
An div linenn-se a zo trawalc'h evit tresañ.
Ouzhpennañ a ran linennoù zo memestra evit bravaat an disoc'h.

Roet em eus an urzh `scale_fill_gradientn()` a servij termenañ al livioù hon eus c'hoant.
Lakaet em eus ur seurt ruz sklaer-mat hag unan a zo teñvaloc'h.
N'eo ket gwal bouezus ar peurrest.

Met n'eo ket echu c'hoazh : skrivet em eus un titl a-drugarez d'an urzh `ggtitle()`.
Evit tennañ ar gael, ar glouet eus ar grafik e ouzhpennan `theme_void()`.
Hag evit echuiñ e termenan stil an titl.

Setu hon disoc'h !
![Kartenn Breizh gant keitad priz ar metrad karrez dre gumun]({static}/media/2021/kartenn_breizh_priziou_dre_gumun.png)
**Kartenn Breizh gant keitad priz ar metrad karrez dre gumun.**

Setu ar skript klok gant kalz a evezhiadennoù : [fardan_ur_gartenn.R]({static}/media/2021/fardan_ur_gartenn.R).

## Klozadur

Setu barrek oc'h da sevel kartennoù gant `R` hag OpenStreetMap bremañ.
Gourc'hemennoù ! 👏

Lavarout a ran en-dro : kinniget em eus **un doare** d'ober.
Doareoù all a zo.
Ha doareoù aesoc'h ivez (gant QGIS da skouer).

Met ar pezh a zo dispar gant `R` a zo fed e c'hellomp cheñch aes-tre an titouroù treset.
Da skouer e c'hellit lakaat `POPULATION` e plas `PrixMoyen_M2` en urzh `ggplot()` ha diskouezet e vo ar boblañs.
Memes tra gant `Nb_Ventes`.

Ouzhpenn se m'ho po c'hoant da dresañ un dra disheñvel-mik diouzh prizioù ar metrad karrez ne vo ket ur gudenn gwall vras.
An hanter eus al labour a vo graet dija : gallout a reoc'h eilañ/pegañ al lodenn gentañ diwar-benn OpenStreetMap.

Gant `R` e c'heller aotomatekaat traoù zo.
Lakaomp eo bet pellgarget roadennoù all gant prizioù ar metrad karrez e Breizh bep bloaz abaoe 2000.
Kentoc'h eget skrivañ an urzhioù gant an dorn ha cheñch un draig evit bep bloaz e c'hellfemp programmiñ ur rodell a rafe ar memes tra evit bep bloaz ha kaout an disoc'h en un teul PDF nemetken da skouer.

---

Emichañs ho peus bet plijadur o heuliañ an tuto-mañ.
Ma n'eo ket sklaer ul lodenn skrivit din mar plij !
Spi am eus n'ez eus ket re a fazioù e brezhoneg ivez.
Gallout a rit mont e darempred ganin dre an evezhiadennoù dindan pe dre bostel m'ho peus c'hoant : blog ατ korr.bzh (lakait "@" e lec'h "ατ").

👋
