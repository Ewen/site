---
title: TIL #8 - Git clean, stash et copier un dépôt git dans un autre dépôt git
date: 2025-01-20
category: Tech
tags: TodayILearned, git, stash, index, ruff, pre-commit
---

## Git : copier un répertoire et son historique dans un autre dépôt
Le contexte de cette petite histoire, c'est que j'ai commencé à travailler à *Ansibiliser* l'infrastructure SIG de la [Redadeg](https://www.ar-redadeg.bzh).
Le contributeur principal m'a suggéré de mettre mon travail dans le dépôt principal du projet.

Mais je suis tellement content de mon historique que j'ai envie de le garder !

Voilà les étapes à suivre.

```bash
# Add my project (local dir) as remote in the main project
git remote add ansible-redadeg ../ansible-redadeg
git fetch ansible-redadeg

# Create a new branch, ansible, with start-point = the main branch in the remote
git checkout -b ansible ansible-redadeg/main
mkdir ansible

# Now, we've got the files from ansible-redadeg, my project
# and we copy those files to what will be the `ansible` dir.
mv Makefile roles ansible.cfg hosts.yml ansible # mv everything
git add .
git commit -m "Import ansible-redadeg"

# Switch back to master and merge with a magical option
git switch master
git merge ansible --allow-unrelated-histories
git mergetool # if any conflicts, like the .gitignore
git commit

# Clean
git remote rm ansible-redadeg
git branch -d ansible
``` 

Et ça fonctionne 🎉

Source :
- [un gentil inconnu sur Stackoverflow](https://stackoverflow.com/questions/1683531/how-to-import-existing-git-repository-into-another/43345686#43345686)


---

## Git : mettre de côté (`stash`) les modifications non indexées (*unstaged*)
Au travail, on a un `pre-commit` qui vérifie avec Ruff que le formatage est correct.
S'il y a un problème, parfois Ruff corrige dans la foulée.
Mais du coup, les corrections ne sont plus dans l'index, et pour peu qu'on ne *commit* qu'une partie des changements, les corrections se retrouvent noyées dans les changements hors index (*undstaged*).

Pour pallier ce problème, je veux mettre de côté les changements non indexés avec la sous-commande `stash`.
Et pour ça, on peut utiliser l'option `--keep-index`.

Ça donne donc :
```bash
git stash --keep-index # ça met tout en haut de la pile "de côté" les modifications
git commit # Ruff détecte et corrige les erreurs
git add -p # je peux enfin voir de quelles erreurs il s'agit
```

---

## Git clean
Une nouvelle sous-commande !

Quand je dois résoudre un conflit, j'utilise à présent `git mergetool`.
J'ai configuré Git pour que ça m'ouvre un nouveau Neovim tout propre avec des fenêtres pour que je puisse comparer les versions.

Mais ça crée des fichiers `*.orig`, qui peuvent servir si on s'est raté dans la génération du conflit.
> After performing a merge, the original file with conflict markers can be saved as a file with a .orig extension.

On a deux solutions pour gérer ces `*.orig` :

- ne jamais les créer, avec l'option `keepBackup = false` dans `~/.gitconfig`.
Pour moi, ça donnerait donc :
```
[mergetool "vimdiff"]
	cmd = nvim -d $BASE $LOCAL $REMOTE $MERGED -c '$wincmd w' -c 'wincmd J'
    keepBackup = false
```

- mais si on préfère garder les fichiers, au cas où, on peut les générer puis les supprimer après coup :
    - jusqu'à présent j'utilisais `rm ./**/*.orig`
    - mais on peut également utiliser `git clean -i`, qui supprime (interactivement avec `-i`) les fichiers non suivis dans le dossier

Une nouvelle sous-commande dans ma besace ! 🎒
