---
title: Lennet em eus : "Bezhin glas - an istor difennet"
date: 2023-08-02
category: Culture
lang: br
tags: lecture, brezhoneg, lennadenn, bretagne
---

Sot on gant ar podkastoù.
Abaoe 2014 a gav din e selaouan podkastoù pe abadennoù radio gant ma fellgomzer (dre [AntennaPod](https://antennapod.org) abaoe meur a vloaz).
Hag abaoe pell zo e selaouan _[Les pieds sur terre](https://www.radiofrance.fr/franceculture/podcasts/les-pieds-sur-terre)_, un abadenn liesseurt war France Culture.

Inès Léraud zo kazetennerez, ha savet he deus meur a reportaj evit _Les pieds sur terre_.
Un heuliadenn he deus kinniget memes : ar _[Journal breton](https://www.radiofrance.fr/franceculture/podcasts/serie-journal-breton-saison-1)_.
Ennañ ez eus kaoz eus industriezh an agroboued e Breizh, ha traoù louz a vez kavet.
Div rann zo gouestlet d'ar bezhin glas, da heul un enklask don a-wac'h he deus kaset da benn.

Al levr a ginnigan hiziv eo disoc'h an enklask ivez.

Brav-tre em eus kavet an tresadennoù, savet gant Pierre van Hove.
Hag evel-just, a-bouez an istor hag enklaskoù Inès Léraud.
Spontus eo gwelet emzalc'h ar pennadurezhioù (ar prefetioù, reoù ar yec'hed, ha kement zo).
Fromet on bet gant istorioù an dud : ar fed n'he deus ket paouezet Inès Léraud da glask pelloc'h.
Diaezamantoù ar familhoù o deus gouzañvet an agroboued, o deus ranket en em gannañ evit gellet lâret ar pezh a felle dezho lâret.

Ar vandenn-dreset a c'heller prenañ e galleg, e brezhoneg hag e gallaoueg.

Ur film zo bet savet diwarni ivez : _[Les algues vertes](https://fr.wikipedia.org/wiki/Les_Algues_vertes)_
Heuliañ a ra istor al levr, hag un nebeud anvioù zo bet cheñchet.
Un doare mat-tre eo evit ober anaoudegezh gant labour dibar Inès Léraud.
