---
title: Lennet em eus : "Pa ra glav" skrivet gant Leire Salaberria
date: 2020-12-11
category: Culture
lang: br
tags: lecture, brezhoneg, lennadenn
---

Skrivet em boa em c'hazetenn ar c'henfinañ #4 em boa prenet levrioù e _Encre de Bretagne_ e kreiz-kêr Roazhon.
Setu lennet em eus unan eus anezho !
Kinniget vo _Pa ra glav_ er pennad-mañ.

_Pa ra glav_ eo skrivet gant [Leire Salaberria](https://leiresalaberria.myportfolio.com/bio).
Ur skrivagnerez hag un dreserez eus bro Spagn / Euskadi eo.
Al levr a oa embannet da gentañ e euskareg (_Euria ari duenean_ eo anv e euskareg).
Troet eo e brezhoneg gant [Riwanon Kervella](https://br.wikipedia.org/wiki/Riwanon_Kervella) ha embannet eo gant an ti-embann [An Alarc'h](http://www.brezhoneg.org/br/catalogues?search_api_views_fulltext=An+Alarc%27h).

Petra eo an istor ?
Simpl a-walc'h eo.
Ul levr evit bugaligoù eo alato !
Emañ ur bugel hag e gi o pourmen er maez.
Glav a ra evel just.
Ar bugel a gont petra a vez graet ganto pa ra glav.
Da skouer pa e pourmenont er maez dindan ar glav en dez ar bugel un disglavier (evel-just).
Hag e gi eo kollet peogwir ne c'hell ket disheñveliñ disglavier ar bugel d'ar re all.
Sapre avantur eo, ne gavit ket ?

Lennet em boa al levr gant kalz plijadur.
Me a blij din ar glav.
Istorioù barzhek evel anezhi a blij din ivez.
An tresadennoù zo simpl a-walc'h met brav kenañ ha "flour" evit an daoulagad.
Mat eo evit ar vugale hag an oadouriena !

Setu tout, kenavo !
