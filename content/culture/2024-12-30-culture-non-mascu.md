---
title: De la culture non mascu
date: 2024-12-30
category: Culture
tags: lecture, société, féminisme, ŒuvreNonMascu
---

Il y a quelques semaines, j'ai écouté la fameuse émission [Libre à vous](https://www.libreavous.org/), l'émission de l'[April](https://april.org) consacrée à la promotion et la défense du logiciel libre.
Dans l'[émission en question](https://www.libreavous.org/222-do-doc-produire-facilement-des-contenus-pyconfr-2024), la chroniqueuse Florence Chabanois pose la question : « Qui a envie d'être sexiste ? »

Si elle pose la question, c'est parce qu'elle a vu à plusieurs reprises l'invisibilisation de certaines personnes dès qu'un homme apparaît. Elle dit : 

> Est-ce que vous aussi, quand un homme apparaît, il fait disparaître les femmes à vos yeux et vos oreilles ? Qu'elles soient des inconnues, vos amies ou des expertes ?
> Est-ce que vous êtes misogyne ?<br>
> Vous ne haïssez pas les femmes… Mais vous ne voyez pas le problème à cette conférence, ce festival, ce concert […], de ne voir que des hommes représentés, qui ne parlent que d'autres hommes, qui ne jouent que leurs œuvres.

Bref, cette chronique a été un excellent levier pour me faire me rendre compte qu'en effet, dans ma consommation culturelle, il y a très peu de diversité.

## Petit inventaire culturel

### 🎵 La musique

J'essaie de donner mon support aux artistes qui produisent la musique que j'écoute, en achetant leurs albums par exemple.
Un des styles musicaux que j'aime beaucoup, c'est la musique bretonne, qu'elle soit à danser (en festoù-noz) ou non.
Il y en a d'autres, heureusement, mais cet exemple est assez flagrant, peut-être parce qu'il s'agit d'un monde assez petit, qui peut donc amplifier les phénomènes de société.

Voilà quelques noms de groupes que j'apprécie bien (classés par ordre antichronologique d'achat du dernier album) :

- [Triskill](https://www.youtube.com/watch?v=bpzLmrXza84) : du "métal à danser" (4 hommes blancs)
- [David Pasquet](https://www.youtube.com/watch?v=_7ZWia65XI0) : un gars qui fait de la bombarde (1 homme blanc, 3 quand il est en trio) 
- [Nâtah Big Band](https://www.youtube.com/watch?v=5qDRR7IdDeo) : un brass band qui fait de la musique bretonne (17 hommes blancs)
- [Krismenn](https://www.youtube.com/watch?v=VTDsTuHJiU0): du rap/beatbox en breton (1 homme blanc, parfois entouré, souvent d'autres hommes)[ref]Bon, après, Krismenn a été directeur artistique de la Kreiz Breizh Akademi, qui fait de vrais efforts de parité.[/ref])
- [Sérot Janvier & la Groove Cie](https://www.youtube.com/watch?v=hxg3nTZhuZw) : une fanfare à danser (5 hommes blancs)
- [O'Tridal](https://www.youtube.com/watch?v=XfTBvkp3r3E) : un trio de fest-noz (3 hommes blancs)
- [Forzh penaos](https://www.youtube.com/watch?v=wsL1gPfuSMA) : un groupe de fest-noz (6 hommes blancs)
- …

Je m'arrête là. Dites-donc, il y a beaucoup de mecs là, non ?

Cette écrasante majorité d'hommes dans ma discothèque se retrouve également sur scène, lors des festoù-noz. Une enquête a été réalisée là-dessus par HF Bretagne[ref][Enquête répartition Femmes/Hommes dans l'enseignement et la pratique de la musique traditionnelle en Bretagne](https://mag.tamm-kreiz.bzh/wp-content/uploads/2021/09/Enquete-HF-musique-trad-bretonne.pdf)[/ref], et quelques statistiques ont pu être collectées pa le site [Tamm-kreiz.bzh](https://tamm-kreiz.bzh), qui recense les festoù-noz et les concerts[ref][https://www.tamm-kreiz.bzh/annuaire-musiciennes](https://www.tamm-kreiz.bzh/annuaire-musiciennes)[/ref] :

- 1 fest-noz sur 3 (29 %) n'a aucune femme sur scène (contre 1% sans aucun homme)
- moins de 5% des groupes sont réellement "mixtes" (50% de femmes au minimum)

Alors évidemment, ce n'est pas la faute de chaque groupe, individuellement.
Moi, j'adore leur musique même !
Mais c'est quand même extrêmement problématique, et c'est quelque chose qui doit être résolu.
Le monde du fest-noz essaie petit à petit de se remettre en question, même si c'est lent.
Je pense à deux petites initiatives :

- Les équipes qui organisent des festoù-noz peuvent tenter d'augmenter la part de groupes mixtes sur leurs scènes.
  Pour cela, le site Tamm-kreiz propose [une page dédiée aux groupes un peu plus paritaires](https://www.tamm-kreiz.bzh/annuaire-musiciennes).
  Bon, il y a beaucoup de groupes constitués de musiciens hommes et d'une chanteuse femme, mais cette petite page a le mérite d'exister.
- Les artistes, elles et eux aussi, peuvent contribuer. Certains artistes hommes, comme Krismenn, refusent de se produire aux événements animés uniquement par des hommes[ref][https://www.letelegramme.fr/bretagne/pour-le-chanteur-krismenn-pas-de-festou-noz-sans-femmes-6527829.php](https://www.letelegramme.fr/bretagne/pour-le-chanteur-krismenn-pas-de-festou-noz-sans-femmes-6527829.php)[/ref]. 


### 📚 Les livres 

J'ai repris goût à la lecture il y a quelques mois.
C'est passé par la relecture d'œuvres qui m'ont marqué quand j'étais ado : la trilogie [À la croisée des mondes](https://fr.wikipedia.org/wiki/%C3%80_la_crois%C3%A9e_des_mondes), de Philip Pullman par exemple.
Et puis, plus récemment, [La Quête d'Ewilan](https://fr.wikipedia.org/wiki/La_Qu%C3%AAte_d%27Ewilan), de Pierre Bottero.

Cette dernière m'a laissé un goût un peu amer.
Si j'étais content de retrouver l'histoire fantastique, mon regard a sans doute un peu changé depuis la dernière fois que je l'ai lue.
J'ai trouvé l'écriture assez lourde, du genre à vouloir placer des mots assez rarement utilisés (et jamais utilisés dans un dialogue).
C'est peut-être parce que je venais de terminer les bouquins de Philip Pullman, qui écrit plus finement à mon goût.

Un autre gros point d'amertume venait du regard masculin (le *male gaze* en anglais), qui se définit comme la représentation des femmes et du monde du point de vue masculin hétérosexuel.
Avec Pierre Bottero, on en bouffe, ma parole, on en bouffe.
Les personnages masculins peuvent être de tout âge, de toute corpulence, plus ou moins con.
À l'inverse, les personnages féminins, absolument tous, sont beaux et même tous sexy, doués, intelligents.
Comme inaccessibles.
Les hommes sont troublés à chaque interaction avec une femme, ou presque.
Les relations sont très stéréotypées, c'est énervant.

Pour avoir un regard un peu plus exhaustif sur ma bibliothèque, j'ai regardé ma collection de livres numériques, pour connaître la proportion d'ouvrages rédigés par des femmes.
Eh bien, à part Rowling, l'autrice d'Harry Potter, il n'y a vraiment pas grand chose.

C'est dingue !

Et c'est extrêmement nul !



### 🎬 Les films et séries
Hop, rapide tour dans ma collection de films et séries.

Ouf, il y a des actrices (coucou Kristen Stewart).
Mais où sont les réalisatrices et les autrices ?

Pas dans ma collection, ou presque pas.

C'est affligeant.


## On fait quoi ?
Moi qui pensais avoir une vision neutre, réaliste, saine de la société.
En fait, quasi toute ma consommation culturelle est fabriquée par un tout petit groupe de personnes. Des hommes, blancs pour la plupart. 

Pourtant, l'art et les œuvres culturelles contribuent à définir notre vision du monde.
On y voit des exemples de sociétés, des exemples de relations humaines. 
On y retrouve des propositions de définition de la morale et tout plein d'autres trucs qui peuvent influencer nos organisations, nos relations, nos objectifs de vie même.

Alors la question du début, posée par Florence Chabanois, prend son sens : « Qui a envie d'être sexiste ? »
Est-ce qu'on a envie de laisser autant de responsabilités à une si petite minorité ? 
Comme elle dit : 
> On chie ce qu'on mange : du sexisme, quand on mange du patriarcat à longueur de journée.


Que peut-on faire ?
> Une façon simple et joyeuse de se démisogyner et d'enfin voir les femmes et leurs œuvres, d'en connaître, de les respecter, de les admirer; c'est d'en consommer explicitement, de manger autre chose.

Alors chiche, pour 2025, je vais essayer de lire, voir, écouter des œuvres réalisées par des femmes seulement.
On peut trouver que c'est extrême, de boycotter les œuvres des hommes. 
Mais après tout, j'ai été gavé pendant plusieurs décennies de leurs œuvres, il y a de la marge pour rétablir un semblant d'équilibre.

En 2025, je tâcherai de partager les œuvres que j'aurais appréciées sur ce blog.
N'hésitez pas à m'en partager, sur Mastodon ([@ewen@mastodon.fedi.bzh](https://mastodon.fedi.bzh/@ewen)) ou par email (blog@*le_nom_de_domaine_de_ce_blog.bio*).
Les notes de la [chronique de Florence](https://www.libreavous.org/titre_podcast_sujet-921) regorgent d'idées de ressources pour se démisogyner.

Je trouve que Florence parle de ça vraiment très bien alors je lui laisse la conclusion :
> Appliquez-vous un quota, soyez paritaire dans votre vie, pour vous comme pour les autres, notamment vos enfants.
> Qu'ils et elles ne pensent pas qu'une fille vaut moins.
> Je crois même que sans ces efforts conscients, dans le monde actuel, nous sommes condamnés à être misogynes et sexistes.
> Goûtez donc cet autre monde, c'est un voyage pavé de pépites.

Merci Florence de m'avoir ouvert les yeux et les oreilles, et bonne année tout le monde !

---

**Edit**, le 2025-01-22 : j'ouvre l'étiquette [ŒuvreNonMascu](tag/oeuvrenonmascu.html) pour recenser les œuvres non mascu que je lis !
