---
title: Gouel Penn-ar-Bed, bloavezh 2023
date: 2023-08-13
translation: true
lang: br
category: Culture
slug: bout-du-monde-2023
tags: festival, musique, brezhoneg, bretagne, finistère
---

Evit an 9vet gwech (bloavezhioù 2011, 2012, 2013, 2014, 2015, 2017, 2018, 2022 ha 2023 !) on bet e gouel Penn ar Ped.
E Kraozon e oa, e penn-kentañ miz Eost.

Er pennad-mañ e vo kontet penaos em eus bevet ar gouel-se.
Kontet e vo eus ar sonerezh hag eus an aergelc'h !

## Gouel Penn-ar-Bed, ur gouel kaer evit an holl

An 23vet gwech e oa eus ar gouel-mañ neuze.
An 23 gwech eo bet aloubet Landaoudeg, e Kraozon, gant tud deuet da selaou sonerezh.

Abaoe 2007 eo stabil an niver a festivalerien·ezed : 60000 en holl, da lâret eo 20000 bemdez.
Ar pezh a vir al lec'h da vezañ re leun, an dud da vezañ re stank.
Ha gallout a reer adkavout mignone·z·ed n'int ket bet gwelet aboe pell.

Ur ger diwar-benn ar sonerezh.
Evel ma c'haller kompren diwar an anv, vez klevet sonerezh a bep seurt eno.
Sonerezh latin, eus Europa ar reter, eus Frañs, funk, kan gant meur a vouezh…
Ha kavet e vez traoù mesket ivez.
E 2017 da skouer eo en em gavet Bagad Ploudiern hag ar Gangbé Brass Band (eus Benin) evit krouiñ ar [Gangbé Breizh Band](https://www.youtube.com/watch?v=fvdGOuctVG0), da vare ar gouel hepken.

An aozañ n'eo ket fall kenebeut !
Teir leurenn zo.
Unan vras, ma vez sonadegoù 1e20.
Ha div vihanoc'h (en o zouez un dogenn/chapito !).
Warno e vez berroc'h ar sonadegoù, 40 mn hepken.
Ar pezh zo, sonet vez div wech warno !
Gras d'an dra-se e c'hell an dud mennet gwelet an holl sonadegoù, en ur vont a bep eil d'al leurennoù.

Skuizhus a c'hell bezañ, met dre chañs ez eus peadra da gavout startijenn.
Tavarnoù zo (e lec'h ma vez paeet gant ar _cashless_ adalek ar bloaz-mañ).
Ostalerioù zo ivez, gant boued a bep seurt ur wech ouzhpenn : krampouezh evel-just, predoù eus bro Brazil, frouezh, boued eus Gwadeloup…

Echu gant an traoù hollek.
Lakaomp ar gaoz war ar bloaz-mañ !

## Ar Gwener

War an dachenn gampiñ en em gavomp gant mignonedezed, e-tro kreizteiz.
Koumoulek eo an oabl met ne ra ket glav.
Aon hon eus evit hor botoù. Deuet omp gant botoù kaoutchouk e ken kas, pri zo memestra.

Lakaat a reomp pailhour (bruzhun liv brav) war hon dremoù.
Kalz a dud o deus graet ar memes tra e gwirionez, un den diwar 25 marteze !
Lonkadenn gentañ hor boeson muiañ karet.
War an dachenn e welomp un nebeud tud anavezet ganeomp ha n'hor boa ket gwelet abaoe pell zo.
Ur wech echu kontañ hor buhezioù eo poent mont da welet ar sonadegoù, digor eo an dorioù !

Treuzwisket eo kalzik a dud.
Ur strollad Simsed zo (gant ar jaoj gwer pe gweroc'h a-us d'o fennoù, hag an Ankoù memes !), ur strollad houidi a blij dezho bourbouilhat er poullennoù…

Strollad kentañ : **Los Wembler's de Iquitos**.
A-bell hon eus gwelet ar sonadeg-mañ.
Ur strollad familh an hini eo, a orin eus Perou, hag a son cumbia.
Reiñ a ra startijenn, gwir eo, met hir ha skuizhus e vo an devezh, ha setu 'giz-se hon eus divizet da chom sioul ha profitañ a-bell.

**Fourth Moon**.
Ar re-se hon eus gwelet a-dost.
Ur strollad eus Frañs/Aostria/Bro-Skos/Italia eo, hag a son muzik iwerzhonek/skosek.
Brav-tre, ha plas a oa evit dañsal ouzhpenn.
N'hon eus ket gellet mirout a zañsal ur c'helc'h sirkasian (ar stumm 2 ha 2), hag ur bourrée !

**Jungle by night**.
Distroet omp war al leurenn vras evit ar strollad-mañ eus ar Broioù-Izel.
Diaes eo termeniñ o stil.
Ur meskaj gant funk, jazz, elektro ?
Ar pezh zo sur eo e oa klavieroù, binvioù kouevr (an trombon en o zouez ❤️), bongoioù ha kongasoù.
Lakaet omp bet da zañsal gant ar sonerezh-mañ !
Kenderc'hel a rin da selaou ar strollad-se, ha pa vefe paotred ennañ hepken.
Unan eus ar rebechoù am eus d'ober ouzh ar gouel an hini eo : kalz muioc'h a baotred evit a verc'hed a vez war al leurennoù.

Da c'houde : ehan !
Fellout a rae deomp mont da welet **Al Qasar**, a oa mat-tre hervez ar pezh hor boa klevet a-raok mont d'ar gouel.
Met war-eeun, a-bell, ne seblante ket bezañ kalz traoù ouzhpenn ar pezh a oa war ar pladennoù.
Hag atav ar memes tra a oa sonet.

Goude-se oamp prest da welet sonadeg ar señorita **Lila Downs** !
Ur ganerez a orin eus Mec'hiko eo. Brudet-kenañ eo e Amerika Latina.
He zonioù zo ur meskaj gant cumbia, folk, ha doareoù all da seniñ eus ar broioù latin.
Kontañ a ra eus traoù a-bouez : ar justis sokial, gwirioù ar merc'hed, ar yezhoù minorel.
N'em eus ket komprenet pep tra war-eeun, dre m'eo deuet da vezañ izel ma live kastilhanek.
Ha n'on ket gouest da gompren ar zapoteg, nag ar mixteg, nag ar mayaeg.
Kanañ a ra gant ur vouezh brav-spontus, asambles gant ur strollad sonerien barrek-kenañ ivez.
Laouen-bras on bet o welet anezhi hag he doare da vezañ war al leurenn !

![5 boul disko, petalennoù tro-dro dezho a oa staliet war an uhel. Sklêrijennet e oant. Al leur a oa leun a pikoù luc'hus.](./boudu_by_night.jpg)
**Kinklet eo bet lec'h ar gouel**.

**Tiken Jah Fakoly**, sonadeg ziwezhañ an devezh.
Ne oa ket displijus kanañ (pe huchal kentoc'h ?) « ils ont partagés le monde… plus rien ne m'étooooonne ».
Ne oa ket dreistordinal kennebeut.
Ha c'hoant kousket hor boa ivez.

Pe kentoc'h, c'hoant hor boa da skeiñ war poubelennoù an dachenn gampiñ.
Evel ma vez graet bep bloaz, hengounel eo.
Deuet oan gant binvioù skeiñ bihan, ha war a seblant e plije an dra-se d'an dud.
Un nebeud re deuzouto a selle ouzhin ha lâret a raent din : « degas a rez un dra ouzhpenn ! »
Roet em boa ur c'hloc'h, un taboulinig, ur re garkabou d'an nebeud tud, ne oan ket ma-unan o skeiñ war draoù ha ne oant ket poubelennoù.
Ne oa ket fall ar strollad-noz-mañ a-benn ar fin !

## Ar Sadorn

Dihunet omp en dachenn gampiñ evit debriñ merenn-vihan… 'ba'r pri.
Glav zo bet e-pad an noz !

**15e24**. Debret ez eus bet ur soubenn ognon. 🤷

**Barrut**, sonadeg kentañ, dindan ar chapito.
Fromus eo bet : ar strollad a gan gant meur a vouezh gant binvioù skeiñ ivez. Kanañ a reont e okitaneg.
War am eus komprenet n'int morse bet ken pell diouzh Okitania.
Mont a raint pelloc'h d'am soñj, ha pa vefe ur strollad arbennik a-walc'h (_de niche_).

**Roberto Fonseca**. Klevet em eus ar soner piano-mañ a-bell hepken.
Kuñv am eus un tamm, rak dispar e oa ar sonadeg hervez 'm eus klevet.
An tonioù oa skrivet brav-tre ha uhel oa live ar sonerien.

**Nana Benz du Togo** zo bet war-lerc'h, ha dreistordinal eo bet.
Teir flac'h zo dirak, ha daou baotr a-dreñv, o skeiñ war binvioù.
Estreget ar c'hlavier bihan zo gant unan eus ar plac'hed n'eus nemet binvioù savet gant an dorn.
Podoù plastik pe houarn evit an taboulinoù, ha tuboù PVC skoet gant tongoù evit seniñ un doare linenn boud elektro.

An pezh a ra trans vodou benel, ha dreist eo.
Un nebeud komzoù fromus zo bet, dreist-holl peogwir eo int a lar an traoù : « Pa lâran ya ez eo ya ! Pa lâran nann, ez eo nann ! »
Ur gerig zo bet diwar-benn ar rummadoù da heul ivez.
Evit an dud a zo war hon lec'h da vevañ mat, e rankfemp ni paouez gant ar pezh a reomp bremañ.

**Lucky Chops** zo unan eus ar strolladoù am boa c'hoant da welet dre ret.
Ur fañfar eus New-York eo, ha deuet eo da vezañ brudetoc'h-brudetañ.
N'omp ket bet dipitet !
Graet em boa anaoudegezh ganto dre o [sonadegoù e metro New-York](https://www.youtube.com/watch?v=c9MiGCQf1bY).
Goude se em boa kavet siouloc'h o pladennoù, ne oant ket ken foll.
Met dirak ar sonadeg vev on bet bamet, ur meskaj etre an traoù fur ha hag an traoù foll e oa. Plijet o deus din !

**Barrut** en-dro, peogwir e oa dispar ar sonadeg kentañ.
Muioc'h a startijenn a oa c'hoazh an eil gwech !

Ur c'housk bihan am eus graet war-lerc'h, berr oa an noz a-raok…

Ha distroet on dirak al leurenn vras evit gwelet **Wax Tailor**, a zo sonerezh elektro hiphop dre vras.

Kempennet-mat e oa : kalzik a dud war al leurenn, punchlines d'ar mareoù mat, ur skramm bras-divent gant videoioù a glote mat-tre gant ar sonerezh.
Betek re am eus kavet.
Ne oa tamm plas ebet evit c'hoari war ar prim.
Gellet hor bije bezañ ur park avaloù-douar e vije bet memes mod evit an arzourien war al leurenn.
Ne oa ket ur gwir darempred etrezomp.

Goude ar sonadegoù on distroet d'an dachenn gampiñ.
Hep mont da welet ar poubelennoù ar wech-mañ.
Tañvet em eus d'unan eus ar gwellañ fritez er bed avat.
Dont a raent eus ar garavanenn a oa staliet war an dachenn, hag a oa digor betek 4e noz bemnoz !

## Ar Sul

D'ar Sul vez dihunet gant ar vombard ha debret vez merenn-vihan gant ur banne laez a teu eus tezh ar saout.
E-giz-se emañ, an hengoun eo.

An hengoun eo mont da welet kenstrivadeg ar re dreuzwisket ivez.
Tri rummad zo : tud o unan, strolladoù, bugale.
Warlene e oa aet ar maout gant unan bennak [a oa tremenet war unan eus al leurennoù ar bloaz-mañ](https://www.letelegramme.fr/culture-loisirs/festivals/bout-du-monde/ce-dimanche-au-bout-du-monde-a-crozon-petit-dejeuner-paysan-et-concours-de-deguisements-au-camping-6405928.php), neuze n'eo ket dister ar genstrivadeg-mañ !

Diouzhtu goude m'eo bet digoret dorioù ar gouel omp bet o welet **Tuuletar**.
Ur strollad a gan gant meur a vouezh eo en-dro, met kanañ a reont-int e finneg.
Ha brav eo bet !
Pep tra zo graet gant o mouezh, hag unan anezho zo gouest d'ober gant ar beat-box.
Ur benveg hengounel bihan o doa degaset ivez, un doare lirenn gant kordennoù rimiet.
Ar strollad, zo merc'hed ennañ hepken, en deus kanet ur [polka anavezet ganeoc'h marteze](https://www.youtube.com/watch?v=qz_uq7ypZnc).

Ha bremañ vo lakaet ar gaoz war ar sonadeg he deus bamet d'am c'halon : **Antti Paalenen gant sonerien skol sonerezh Brest**.
Ur paotr finnek azezet war ur gador eo Antti Paalanen.
Sonañ a ra akordeoñs ha kanañ a ra vogalennoù gant ur vouezh don-don.
Kinniget e-mod-se ne seblant ket bezañ dibar.
Ha padal.
Padal tres un den tre zo warnañ.
Padal eo fromus ar pezh a lâr, gant an holl fleurioù un tamm pep-lec'h war al leurenn.
Hag ur gwir liamm en deus savet gant ar publik.
Lañset en deus unan eus e heuliadennoù vogalennoù war un ton da skouer ([da glevet amañ evit kompren](https://www.youtube.com/watch?v=oba-I_i6Pjg)).
Goude an ton-mañ eo en em gavet an tabouliner.
Antti Paalanen en deus prezañtet anezhañ, hag a-raok m'en doa echuet e oa krog ar publik da ganañ ar vogalennoù en-dro.
Hag eñ da lañsañ un taol-lagad d'an tabouliner, gant ur mousc'hoarzh bras-divent, ha dao ! gant an ton kent.
_On fire_ oa ar publik.

Hag un dra all a oa gwelloc'h c'hoazh : c'hoant en deus bet d'ober unan eus ar meskajoù burzhudus a c'hoarvez e gouel Penn-ar-Bed, goude ur sonadeg en doa graet e 2022.
Bet e oa o pleustriñ meur a wech e Brest al bloaz-mañ evit prientiñ un dra bennak gant studierenezed ar skol sonerezh.
Kaout c'hoant d'ober un dra e-giz-se oa dibar dija.
An disoc'h avat a oa dreistordinal ha fromus-tre.
Kanerienezed ha sonerienezed akordeoñs o deus lañset gantañ [gwerz ar vezhinerien](https://www.youtube.com/watch?v=IN6iSxm4TO8).
Pwa pwa pwa… an diskan gant an akordeoñs burzhudus a oa fromus.
Ha peurrest an ton, kaer-meurbet.

Aes eo bezañ piket h·e c'h·g·alon gant tud estren a blij dezho hor sevenadur deomp-ni.
Hag a deuont a-benn d'ober traoù ken brav ouzhpenn.
Ur gavotenn zo bet goude-se memes, ha me 'lâr deoc'h, n'eus ket bet kalz gavotennoù e Landaoudeg !
Mersi bras dit, Antti Paalanen <3

Goude un ehan hir a-walc'h evit diskuizhañ ha distreiñ war an douar, en ur zebriñ frouezh en disheol, omp bet o welet **Makoto San**.
Ar re-se 'oa ur strollad hor boa c'hoant da welet ivez, hag a ya da vezañ brudetoc'h-brudetañ.
Seniñ a reont elektro gant un aergelc'h japanek (ha pa vefe gall ar strollad).
Ar pevar den, masklet o fenn mod _Squid game_, a sko war tammoù bambou, gant sonerezh elektro ouzhpenn.
Un tamm rebech bihan adarre : mankout a ra ul liamm gant ar publik.
Graet eo bet a-ratozh moarvat.
Lakaat a reont maskloù evit bezañ tudennoù all, kevrinus, hag abalamour d'an dra-se ne vez ket komzet ouzh ar publik.
Ar santimant am eus bet e oa o zonioù ur restr MP3 o treiñ a-dreñv, petra bennak ma raent gant an tammoù bambou e-kichen.
Koulskoude e oa brav-tre an tonioù, pardonet int !

N'em eus ket miret un eñvorenn dreistordinal eus **Total Hip Replacement & Anyankofo**.
An trede devezh e oa memestra, ha n'on ket sot gant ar sonerezh soul/reggae.
Farsus eo o anv avat ! (nag un vurutelladenn arzel dreist ! — Ma blog eo, lâret a ran ar pezh am eus c'hoant da lâret !)

**Louise Attaque** zo bet hor _joker_.
Pa oa 80% eus an dud o c'haloupat war-zu al leurenn vras… hor boa ar peurrest evidomp !
Ar privezioù hag ar boued hep lost, peadra d'en em azezañ… Dreist eo bet.

**Meute** oa ur strollad hor boa c'hoant da welet ivez.
Sonet o doa e Landaoudeg un nebeud bloavezhioù zo dija.
Hag efedus int : ur fañfar eo, ha pa vezont war al leurenn e roont ar santimant e sonont elektro.
Lakaet vez kreñv ar boudennoù, ar marimbaoù a son traoù harpet gant ar binvioù skeiñ hag ar binvioù kouevr.
Sikouret o deus ac'hanomp da chom dihunet !

Goude-se hon eus graet un tamm tro war-zu **Makoto San** en-dro, peogwir e sonont brav memestra.

Hag echuet hon eus gant ar sonadeg ziwezhañ : **Smokey Joe and the Kid**.
Hip-hop / elektro eo, ur show divyezhek galleg / saozneg, ha brav eo bet.
Deuet int a-benn da c'hoari o roll : echuiñ, klozañ ar gouel, gant startijenn.
Ur gwir darempred oa gant ar publik. Da skouer o deus goulennet, sirius-tre : « Mankout a ra piñata Gérald Darmanin amañ, 'ra ket ? »

## YOLO

O tistreiñ e oan d'an dachenn gampiñ pa 'z on bet galvet gant tud zo o deus goulennet ganin ha komz a raen brezhoneg.
Fellout a rae dezho degemer an dud a yae d'ar privezioù gant troiennoù divyezhek (« demat », « kalon vat », « noz vat »…).

A-benn ar fin on distroet d'ar poubelennoù evit kemer perzh er _gwir_ sonadeg ziwezhañ, hag o kousket e oan just a-raok ma savas an heol.

## Evit echuiñ

Unan eus ar bloavezhioù gwellañ eo bet ar bloavez-mañ.
Strolladoù a bep seurt a oa, plijadur zo bet gant tout an dud.
Gellet em eus ober anaoudegezh gant strolladoù brav-tre, un nebeud anezho a gendalc'hin da selaou anezho, sur eo ha n'eo ket marteze !

Bez e vez traoù ha n'eus deuzouto nemet war al lec'h, war-eeun.
Ha traoù zo bet a oa deuzouto er Boudu 2023 hepken.

Met traoù all a vo e 2023 😍
