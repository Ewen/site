---
title: Lecture : Comment devenir moins con en dix étapes
date: 2025-01-27
tags: lecture, féminisme, masculinisme, #ConseilPodcast, société
category: Culture
---

Aujourd'hui je vais vous parler d'un bouquin que j'ai lu, suite à l'écoute du podcast des Couilles sur la table[ref]Un épisode en deux parties : [partie 1](https://www.binge.audio/podcast/les-couilles-sur-la-table/guide-pratique-pour-devenir-un-vrai-mec-bien) et [partie 2](https://www.binge.audio/podcast/les-couilles-sur-la-table/guide-pratique-pour-devenir-un-vrai-mec-bien-2-2).[/ref] (je recommande 1000 fois ce podcast, et en particulier cet épisode !). 
Il s'agit de [*Comment devenir moins con en dix étapes*](https://horsdatteinte.org/livre/comment-devenir-moins-con-en-dix-etapes/), par Quentin Delval, publié en 2023 par les éditions Hors d'atteinte, une maison d'édition féministe de fiction et de non fiction.

Alors oui, c'est une entorse à mon [défi de ne lire/écouter/regarder que des œuvres réalisées par des personnes qui ne sont pas des hommes cisgenres](/de-la-culture-non-mascu.html#:~:text=Alors chiche, équilibre.). 
Mais j'avais commencé le livre avant début 2025, et ça va un peu dans le même sens, mine de rien.
Et puis ça m'a fait découvrir une maison d'édition qui regorge de pépites, je crois que je vais piocher dans ses livres cette année !

[![Couverture du livre Comment devenir moins con en 10 étapes]({static}/media/2025/devenir_moins_con.jpg){: .image-process-article-image }]({static}/media/2025/devenir_moins_con.jpg)
/// caption
Couverture du livre *Comment devenir moins con en 10 étapes*.
///

Donc écrivais-je, j'ai lu ce livre. 
Je l'ai dévoré même. Et je pense que je vais le relire plusieurs fois dans ma vie.
Il a eu l'effet d'une petite bombe, il m'a ouvert les yeux sur pas mal de choses, au sujet de mon comportement ou bien le comportement d'autres personnes que je côtoie.

## Pourquoi ce livre ?
Peut-être qu'on peut commencer par parler de la raison d'être du livre.

Les femmes font, chaque jour, un gros travail de pédagogie, d'explication auprès des hommes, pour parler féminisme. 
Par un concours de circonstances, l'auteur commençait à réfléchir à ces sujets et écrivait là-dessus sur un blog.
La maison d'édition Hors d'atteinte lui a alors fait commande de ce livre, pour qu'au moins une fois, ce soit un homme qui s'empare du sujet et qui *fasse le taf*.

Le livre s'adresse aux hommes cisgenres[ref]une personne cisgenre est une personne dont l'identité de genre correspond au genre assigné à sa naissance.[/ref] hétérosexuels.
Notamment ceux qui peuvent se trouver en couple, mais l'auteur prend du recul et parle souvent du rapport que les hommes ont avec les femmes en général, un peu partout : en famille, au travail, avec des amies, dans une asso, etc.

Ce n'est pas un manuel d'autoflagellation, et le ton n'est pas du tout condescendant.
L'auteur parle plutôt de son vécu personnel et professionnel, et essaie de tirer les grandes lignes de ce qu'il voit autour de lui.

Personnellement, j'ai trouvé que ça mettait des mots sur des comportements que j'ai ou ai pu avoir, ou bien des comportements d'hommes que j'ai pu voir autour de moi.


## Alors, ça parle de quoi, concrètement ?
Je ne vais pas en faire un résumé hyper détaillé ici.
Mais voilà en gros la structure du livre, et quelques passages que j'ai trouvés particulièrement intéressants, dans mon cas personnel.

/// details | Cliquez pour dérouler la structure du livre

### 1. Découvrir qu'on est un homme
Ce chapitre parle de l'éducation qu'ont les enfants.
Les garçons suivent l'exemple de leur papa, (trop) souvent peu émotif, censé incarner la stabilité, travailleur, actif…


Et aussi il introduit le concept de *l'esquive*. 
C'est-à-dire un capacité partagée par de nombreux hommes à décider de ce qu'on fait ou non.
On le fait tellement bien que c'est assez accepté dans la société.
Si dans un groupe on s'occupe moins des enfants ou de la popote, c'est comme ça, c'est pas grave. 
Consciemment ou inconsciemment, on pense : *"de toute façon il y aura sûrement une femme responsable pour s'en occuper !"*

### 2. Redonner leur place aux émotions
Gros sujet ici.
Les hommes (quand je dis *les hommes* c'est, encore une fois ici, les hommes cisgenres hétérosexuels, en moyenne) ressentent des émotions, mais elles sont plutôt peu riches. 
L'auteur parle de deux extrêmes : la colère (*"vazy wech tu ma doublé sur la route avec ta voiture, sa m'enerve laaaa"*) et "faire le saint", c'est-à-dire rester calme face à toute situation.
On peut passer d'un extrême à l'autre, sans rien résoudre. 
Avouez que ce n'est pas très constructif de crier de colère, ou de dire mielleusement *"non mais si, ça va ma chérie, ne t'inquiète pas"* alors qu'en fait on a quelque chose sur le cœur.

Quelque chose sur le cœur, qu'on est parfois foutrement incapable de nommer, et ça m'est déjà arrivé plusieurs fois.
Et quand je vois des copines qui parlent de leurs émotions, parfois je me sens avoir la capacité émotionnelle d'une petite cuillère (et ça me fait penser à [cette référence dans Harry Potter](https://youtu.be/r94aUPdPgzk?feature=shared&t=23)).

L'auteur remet donc au centre les émotions, que c'est normal de les ressentir.
Qu'il faut les accepter, les embrasser. 
Et qu'on peut travailler à les reconnaitre.
Il y a même des outils, comme [la boussole des émotions](https://emotioncompass.org/fr/) ou [la roue des émotions](https://www.developpementpersonnel.fr/wp-content/uploads/2022/03/roue-des-emotions.png) (une parmi d'autres).


### 3. Découvrir son corps
Dans ce chapitre, l'auteur nous rappelle que le rapport au corps est différent, selon que l'on est homme ou femme.
Le regard des autres personnes sur notre corps est différent.
La physiologie est différente, je pense aux cycles menstruels qui arrivent puis repartent, l'accouchement…

L'auteur raconte bien mieux que moi, mais voilà deux exemples intéressants qu'il propose :

- il parle des maladies passagères (grippe, fièvre…). Les hommes, en général, utilisent cette condition pour esquiver les tâches. Ou bien s'ils font la tâche, c'est en traînant des pieds. Les femmes non plus n'aiment pas le faire mais *il faut le faire*, donc elles le font.
- pour les maladies plus longues, plus graves, là aussi il peut y avoir une différence entre femmes et hommes. Les hommes attendraient plus longtemps avant de consulter, voire le feraient sur conseil (ou demande) de leur partenaire (ça aussi, ça m'est arrivé à moi).

### 4. Découvrir l'empathie
On parle ici d'empathie, sans surprise. 

On a tous plus ou moins l'impression, je pense, d'être capable de se mettre à la place des autres.
Mais en fait, il y a plusieurs niveaux, et grosso modo, les femmes sont meilleures.
La bonne nouvelle, c'est que *"l'empathie n'est pas innée, elle s'apprend"*.

Dans un exemple autour de la gestion des émotions de leurs enfants, l'auteur compare son empathie à celle de sa compagne. Il en conclut :

> En résumé, mon empathie consiste à réagir à ce que je vois sur le moment, et si ça ne fonctionne pas, à me plaindre que l'autre exagère.
> Son empathie à elle consiste à partir d'une observation fugace, à en déduire dans quel état est la personne, à comprendre les enjeux de la situation, puis à anticiper celles qui vont suivre pour bien accompagner ladite personne.

Pas tout à fait pareil.

L'auteur évoque une potentielle raison de cette différence : *"les femmes sont plus souvent empêtrées dans des situations qu'elles doivent gérer."*
Elles prennent soin des autres, et doivent souvent anticiper davantage pour que tout se passe bien.


### 5. Écouter les femmes (toutes les femmes)
On voit dans ce chapitre que dans plein plein de cas, les hommes n'écoutent pas les femmes :

- jouer à un jeu vidéo et dire *"oui, oui"* à sa copine/mère/amie
- ne pas considérer ce que dit une femme au travail, attendre que ce soit un homme qui le dise pour finalement être d'accord
- quand on dit à une femme *"nan, mais tu exagères là"* alors qu'elle vient de se livrer

Et tant d'autres situations (le coup du vieux gynéco qui s'adresse à l'homme pour parler de la santé de sa copine, alors qu'elle est juste à côté, on vous l'a déjà fait ?).

En réponse, l'auteur donne un petit tuto sur l'écoute active :

- se rendre disponible à l'autre, lui faire face, être calme
- ne pas montrer d'agacement, de jugement, d'agitation
- ne pas parler tant que l'autre n'a pas terminé
- ne pas conseillé si ce n'est pas souhaité

Comme l'auteur, j'ai souvent tendance à vouloir absolument trouver une solution cartésienne à un problème, si on m'en fait part.
Alors que la personne en face a probablement juste envie de partager ce qu'elle a sur le cœur, et que les solutions cartésiennes, elle y a déjà sans doute pensé.


### 6. Prendre sa part
On revient à la fameuse esquive du début du bouquin. Avec plusieurs techniques, peut-être que vous en retrouverez quelques-unes dans votre comportement ou le comportement d'un proche :

- la délégation implicite : *"qu'est-ce que je peux faire pour t'aider ?"*
- le *"je fais déjà beaucoup !"*
- l'altruisme autocentré : *"je bosse beaucoup et on me voit pas à la maison, mais c'est pour vous que je fais ça !"*
- des beaux lendemains : *"j'allais le faire !"*
- etc

Autant d'esquives qu'il faut arriver à identifier et démonter.
Ça a des avantages quand même : devenir plus fiable former une vraie équipe !

### 7. Agir concrètement
Ça parle de volonté de changement, de trouver des modèles, et de quelques conseils pratiques, comme des réunions de coordination sur les tâches ménagères ou de partage émotionnel.

### 8. Échouer, être ridicule et y survivre
Le principal point de ce chapitre, c'est qu'il faut arrêter de se comparer à moins bon que soit pour se dire qu'on est déjà un mec bien.
Les exemples, les modèles, ils sont plutôt à prendre chez les hommes qui font mieux, et évidemment chez les femmes.

La première fois qu'on fait une tâche ménagère, qu'on prend les devant pour organiser telle sortie ou telle activité, on peut se planter. On peut être pataud.
Mais c'est carrément mieux que de ne rien avoir fait.
On doit faire des erreurs pour progresser.


### 9. Vivre les autres hommes
Tout le bouquin parlait surtout des relations avec les femmes : comment former une équipe avec une partenaire, écouter une collègue de boulot, etc.
Mais les autres hommes n'ont pour autant pas disparu, alors qu'est-ce qu'on fait avec eux ?

L'auteur parle de *Devenir traître, mais garder ses couilles sur la table* (en référence au podcast, bien entendu) : sortir de la compétition masculine et assumer. 
Et ça peut ne pas être facile, certains hommes peuvent reprocher des choses plus ou moins clairement (*"ma copine me demande de nettoyer les chiottes à cause de toi"*).

Un autre rôle qu'on peut se donner, c'est de faire gaffe à ce que les autres hommes disent.
De signaler quand quelqu'un dit quelque chose de pas ok. Ça peut aller du simple étonnement (répondre à un gars qui dit *"t'façon les femmes c'est fait pour s'occuper des enfants"* : *"Ah bon, t'es sûr ?"*).
Au *mansplaining* (l'arroseur arrosé, à qui on dit : *"attend, laisse-moi t'expliquer un truc"*).
Jusqu'au Adèle Haenel : *"Je suis pas ok avec ça du tout, je me casse."*


Et puis, l'auteur suggère une idée. L'idée de constituer des groupes, des cercles d'hommes pour parler de ces questions.
Il explique un fonctionnement possible, ou chaque semaine, des hommes se retrouvent pour parler de leur comportement, de choses qu'ils pourraient améliorer.
Identifier collectivement des comportements mauvais.
Ça m'intéresse pas mal, cette idée. 👀

### 10. *** 
Et hop, je laisse le nom du dernier point complètement dans l'inconnu, en espérant que ça vous donnera envie de vous procurer le livre et de le lire !
///


## En conclusion

J'ai vraiment beaucoup aimé cet ouvrage.
Il met des mots sur des sentiments, des comportements vécus ou vus.
Le plus souvent, dans une relation hétérosexuelle, mais parfois aussi simplement avec des amis, ou tout autre groupe de gens.

Non seulement le constat est assez clair, mais en plus le livre arrive avec des pistes de solution.
Pour l'instant, très individuelles, mais l'objectif du livre ne va pas au-delà et c'est déjà largement suffisant, comme nourriture intellectuelle à ingérer.

À défaut de lire le bouquin, on peut aussi simplement écouter l'épisode en deux parties qui y est consacré par le podcast des Couilles sur la table : [Guide pratique pour devenir un vrai "mec bien"](https://www.binge.audio/podcast/les-couilles-sur-la-table/guide-pratique-pour-devenir-un-vrai-mec-bien).
Tout le monde ne peut qu'apprendre des choses !

Si des lecteurs, lectrices, voudraient discuter de ce livre et de tout ce qu'il y a autour avec moi, n'hésitez pas à me contacter sur [Mastodon](https://mastodon.fedi.bzh/@ewen) ou par email !
