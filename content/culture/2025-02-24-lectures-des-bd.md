---
title: Lecture : les bandes dessinées de l'hiver
date: 2025-02-24
tags: lecture, bd, ressources, genre, guerre, histoire, anglais, ŒuvreNonMascu
category: Culture
---

👋 Allez hop, un petit article sur mes dernières lectures !


À Noël j'ai reçu quelques BD, et j'ai donc commencé mon année de lecture (que j'espère bonne !) en les lisant.
Petit tour de bibliothèque.

## *Ressources, un défi pour l'humanité*, de Philippe Bihouix et Vincent Perriot
La première BD de 2025, c'est… une BD qui a cassé mon défi de 2025 de ne pas lire, regarder, écouter des œuvres réalisées par des hommes cisgenres (cf mon [article sur la culture non mascu](/de-la-culture-non-mascu.html)).
Mais comme ça c'est fait, je peux vraiment me concentrer sur le défi sur le reste de l'année ! Et puis il s'agissait d'un cadeau…

J'ai hésité à mettre la BD de côté et ne la lire que l'année prochaine, mais maintenant qu'elle a été imprimée et qu'elle se retrouve dans ma bibliothèque… Ç'aurait été du gâchis !

[![Couverture du livre *Ressources, un défi pour l'humanité*]({static}/media/2025/ressources.jpg){: .image-process-article-image }]({static}/media/2025/ressources.jpg)
/// caption
Couverture du livre *Ressources, un défi pour l'humanité*.
///


Et de gâchis, c'est justement ce dont il est question dans *Ressources*.
Car Philippe Bihouix est ingénieur, spécialiste des ressources minérales et promoteur des *low-tech*.

Le principe de la BD, c'est la rencontre de cet ingénieur avec un dessinateur, Vincent Perriot.
Et les deux apportent leurs compétences pour faire cette BD (quelle mise en abîme, alors !).

On apprend plein de choses, mais personnellement je n'ai pas trop vu de lien entre l'histoire, ou plutôt le contenu, et les dessins.
Les dessins apportent indéniablement une facilité de lecture. C'est aérien, on voyage dans le temps, l'espace, l'infiniment petit, l'infiniment grand.
Mais j'ai ressenti cette déconnexion entre dessin et contenu donc, 
peut-être alimentée par une sorte de gêne due au même sourire neutre que les deux personnages arborent en permanence.

Voilà pour le râlage.
Je suis quand même très content d'avoir lu cet ouvrage.
J'ai appris pas mal de choses.

Je savais que les ressources, sur notre planète Terre, sont limitées.
J'avais plus en tête les ressources énergétiques, j'avoue (d'où l'intérêt de créer des réseaux et des protocoles résilients, qui n'ont pas besoin d'être connectés en permanence, qui fonctionnent avec des petites ou grandes interruptions, tout ça tout ça).
Mais ce que je sous-estimais largement, c'était les autres ressources.
La matière. 
Les métaux, les terres rares, etc.
Ce sur quoi l'humanité, notre société en tout cas, s'est bâtie.
Il y a un gros chapitres sur les mines, par exemple, très intéressant.
Cette partie fait d'ailleurs écho à [un épisode du Pain et des parpaings](https://hearthis.at/radiopikez/dpedp22-nominaran2/) de [Radio Pikez](https://pikez.space), consacré aux mines, en Bretagne et ailleurs, où on retrouve pas mal d'infos du bouquin.

Pas une BD très réjouissante, mais au contenu très intéressant donc.

## *Mauvais genre*, de Chloé Cruchaudet
Cette BD est une adaptation du roman *La garçonne et l'assassin* de Fabrice Virgili et Danièle Voldman, qui lui-même est tiré de faits réels.

[![Couverture du livre *Mauvais genre*]({static}/media/2025/mauvaisgenre.jpg){: .image-process-article-image }]({static}/media/2025/mauvaisgenre.jpg)
/// caption
Couverture du livre *Mauvais genre*.
///

Le résumé à gros traits, c'est un gars, Paul, qui n'en peut plus des tranchées de la Première Guerre mondiale.
On le comprend. 😬

Il déserte, retrouve sa femme, avec qui il venait tout juste de se marier.
Sauf que les déserteurs, surtout pendant la guerre (et pendant plusieurs années après), ils sont pas très bien vus.
Alors il doit se cacher. 
D'abord il reste dans une chambre d'hôtel, puis il en a marre.
Il s'habille en femme pour sortir.

Et, petit à petit, change d'identité, à l'extérieur en tout cas.
La BD parle de cette transition, de la relation de couple qui évolue, de leur rapport avec le reste de la société.

J'ai beaucoup aimé, parce que ça questionne le genre, à une époque où, je n'avais jamais soupçonné que ce genre de choses arrivaient (quel suspense !).
Ça parle un peu de sexualité aussi.
De masculinisme et de violences faites aux femmes, de guerre.

De belles archives servent d'épilogue, c'est vraiment beau.

Je recommande chaudement.
Et peut-être que je demanderai pour le prochain Noël le roman qui a inspiré le livre !


## *The boy who lost his spark*, de Maggie O'Farrell
Cette BD, on ne me l'a pas offerte, mais je l'ai empruntée dans à la *médiathèque de ma ville*. Ça fait teeeeellement longtemps que j'avais pas fait ça ! Je suis refait.


[![Couverture du livre *The boy who lost his spark*]({static}/media/2025/theboywholosthisspark.jpg){: .image-process-article-image }]({static}/media/2025/theboywholosthisspark.jpg)
/// caption
Couverture du livre *The boy who lost his spark*.
///

Bon, comme son titre l'indique, ce n'est pas exactement une BD en français. 
J'étais étonné de voir de l'anglais à la médiathèque, l'image était très belle, alors j'ai emprunté ce livre !

Ce n'est pas vraiment une BD, plutôt un livre jeunesse avec des images. 
De très belles images.
Ça raconte l'histoire d'un jeune garçon, qui a déménagé de la ville à la campagne avec sa mère et sa sœur.
Et sur la colline sur laquelle se trouve leur maison se passent des choses étranges.

Je n'ai pas spécialement accroché à l'histoire.
Peut-être que c'est parce que je ne savais pas à qui était destiné l'histoire.
C'est quand même très chouette, mais ça ne m'a pas touché plus que ça.

Par contre, les dessins… Une belle claque.
La dessinatrice, Daniela Jaglenka Terrazzini, a réalisé de superbes planches.
À la fois réalistes et poétiques.
Et, pour le coup, les dessins accompagnent et rythment vraiment bien l'histoire. 

[![Une page du livre *The boy who lost his spark*]({static}/media/2025/theboywholosthisspark2.jpg){: .image-process-article-image }]({static}/media/2025/theboywholosthisspark2.jpg)
/// caption
Une page du livre *The boy who lost his spark*.
///

Les magnifiques dessins font qu'on rentre bien dans l'histoire, c'est beau.


---

Et c'est tout pour cette fois. 
Rendez-vous lorsque j'aurai lu ma prochaine brochette de BD ! 👋
