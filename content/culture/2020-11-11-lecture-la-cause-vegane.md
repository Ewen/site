---
title: J'ai lu : « La cause végane » de Frédéric Denhez
date: 2020-11-11
category: Culture
tags: lecture, véganisme, société
---

J'ai récemment lu un livre sur le véganisme.
Ou plutôt contre le véganisme, vu que l'auteur semble plutôt prendre parti contre cette façon de vivre.
Certains arguments me dérangeaient pas mal, alors je me suis dit que j'essaierai de donner un avis sur ce livre.
Et puis le reconfinement est arrivé, alors j'ai eu un peu de temps pour rédiger ça !

## Résumé et principaux arguments

Alors, je vais commencer par un petit résumé que j'espère objectif, avec les principaux arguments évoqués, contre la cause végane.

Tout d'abord, l'auteur commence par une petite introduction sur le mouvement végan.
Ne connaissant pas son historique, je ne peux pas dire s'il s'agit de [morceaux choisis](https://fr.wikipedia.org/wiki/Cherry_picking) ou d'un résumé objectif.
En tout cas je l'ai trouvé assez correct, j'ai appris des trucs.
Il continue avec quelques données factuelles sur les effets actuels de la consommation (excessive) de viande, pour l'environnement.
Plutôt propre, je trouve.

Ensuite, on rentre dans le dur.
Par l'intermédiaire de plusieurs personnes interviewées, il explique que le véganisme est un effet de mode, en tout cas en partie.
Que c'est un mouvement amplifié par les médias, qui peut se rapprocher des modes du _healthy food_, de l'[orthorexie](https://fr.wikipedia.org/wiki/Orthorexie), cette injonction à manger parfaitement.

Il passe ensuite un bon petit moment à expliquer les liens potentiels entre véganisme et religion ; j'y reviendrai plus tard.

Après ça, il évoque l'élevage, en compagnie de quelques éleveurs avec qui il s'est également entretenu.
Pour eux, l'élevage a du bon, il permet de préserver des écosystèmes, de valoriser et d'entretenir des zones riches en biodiversité.
Et c'est sans compter l'intérêt des déjections animales pour fertiliser les sols cultivés.

> « Dans ce milieu âpre, les paysages rêches – de la lande surtout – sont maintenus depuis deux cents ans par la culture ovine. Sans les moutons, sans le pâturage, la bruyère disparaîtrait, le genêt serait supplanté par des boisements de pruneliers et de poiriers sauvages qui fermeraient les horizons au bénéfice d'une biodiversité animale très frustre. […] Sans [l'élevage], on perd les oiseaux comme le circaète Jean-le-Blanc, qui aime les milieux ouverts ; sans lui, on ne cuillerait pas de plantes médicinales. »

Dans le chapitre suivant, consacré à l'assiette, concrètement, il a un propos plus mitigé.
Tantôt il considère le véganisme encore comme une mode, un argument marketing ; il écrit également qu'il serait possible de vivre sans produits animaux, mais qu'il faudrait alors quantité de suppléments alimentaires, encore au profit de grosses industries.
Tantôt il donne la parole à des professionnel·le·s de santé, qui ont l'air de dire qu'on peut vivre sans produits animaux, sans trop trop de difficulté.
Il parle également de la viande _in vitro_ dans ce chapitre, en évoquant ses dangers et ses dérives.
D'une part, ce genre d'alimentation génère (et générerait encore plus) des problèmes environnements.
D'autre part, selon lui, les végan·e·s sont les « naïfs utiles du système ».

Enfin, dans le dernier chapitre, l'auteur s'entretient avec l'un des cofondateurs de l'association L214.
Si l'on sent un peu de mauvaise foi (_« J'ai été surpris qu'il accepte de me rencontrer […] »_), le chapitre est globalement calme et très instructif.
On comprend la génèse de l'association, sa stratégie, ses positions.

Voilà en gros le contenu du livre, maintenant passons à ce qui m'a le plus rebuté.

## Mes critiques

Cette partie est nettement plus subjective que la prédécente.
Je reprends des passages dans l'ordre, et je les commente.

> « L'homme a toujours été cruel, c'est dans sa nature. […] La violence, la cruauté, le massacre, la torture, le viol sont des dispositions propres à notre espèce. »

Premier bel amalgame.
Tous les humains sont cruels, sans exception, vraiment ?
Si beaucoup de monde aime _Game of Throne_, cela veut-il vraiment dire que tout le monde est cruel et veut décapiter son prochain (oui oui, cet argument est utilisé) ?
Mon avis, c'est que certes, il existe des individus violents.
Mais qu'en société, on arrive tout même à gérer ça, globalement.
Si on va dans son sens, et qu'on fait des amalgames comme lui, vu qu'on mange des animaux, on devrait ouvrir également des restaurants cannibales, tant notre cruauté est grande.

---

> « Les végans n'ont pas de projet politique. La question que je me pose, c'est pourquoi ils plaisent aux médias. […] Pourquoi autant de battage ? »

Qu'entend-il par politique ?
Les militant·e·s végan·e·s n'essaient-iels pas de changer la société, ou en tout cas de poser des questions sociétales ?
C'est à mon avis un projet politique suffisamment vaste.

---

> « Quand on réfléchit bien, on ne mange pas des animaux pour se nourrir, mais pour avoir des relations sociales. Dans notre culture, on partage un bœuf bourguignon, avec du vin, c'est du lien social ; tant que ces mouvements ne créeront pas d'autres liens sociaux autour du tofu… Il faut qu'ils inventent un autre imaginaire. »

Pour moi, cette question elle est vite répondue.
Par exemple, le partage de recettes véganes se fait largement, du fait des contraintes d'ingrédients.
Ce partage de connaissances est un beau lien social je trouve.

---

Après nombre de références religieuses, l'auteur écrit :

> « Que l'on soit croyant ou pas, cette culture nous imprègne depuis qu'elle existe. Même si l'on est athée, comme moi, on ne s'émancipe pas totalement de 2000 ans de judéo-christianisme. »

Peut-être ne suis-je pas sensible à cette symbolique, mais en tout cas je comprends pas.
D'autant qu'il fait plein d'allusions dans le chapitre à des références religieuses, pour illustrer son propos.
Malgré ses tentatives, je ne comprends pas le lien entre la religion et le véganisme.

Et attention, dans la même section, les arguments deviennent de plus en plus [WTF](https://fr.wikipedia.org/wiki/Liste_de_termes_d%27argot_Internet#W,_X,_Y,_Z) :

> « […] On trouve, cela dit, des « végésexuels » qui ne veulent avoir de relations sexuelles qu'avec leurs semblables, afin de ne pas être contaminé par les fluides corporels des omnivores, ou, pis, par ceux des carnivores. »

Et donc, il trouve une thèse sur le sujet et la cite abondamment, notamment pour définir le véganisme :

> « L'individu végan regarde le monde social au prisme d'une bipartition de l'ordre symbolique dans lequel, d'une part, il y a l'ensemble des individus "carni" ou "omni" (-vores) qui appartiennent à la sphère du mal, du souillé, de l'impur, et encore expriment la chute, la compromission, le barbare et la sauvagerie, de l'autre, il y a les "végé" éthiques et végans qui eux appartiennent à la sphère du bien, du sain et du pur, expriment encore l'élévation et la civilisation. »

Au-delà du nombre de virgules que compte cette phrase (onze), il y a un gros problème de généralité et d'amalgame.
C'est bien de vouloir définir un mouvement, un groupe de gens, mais des nuances peuvent exister.
Cette définition est très fermée et même violente pour les personnes concernées.

---

Toujours dans ce chapitre autour du puritanisme (et donc toujours en rapport avec la religion, d'après la [page wikipédia](https://fr.wikipedia.org/wiki/Puritanisme) consacrée), on parle science.
Et on arrive au point qui m'a donné vraiment envie d'écrire une critique de ce livre, car pour moi, ça va vraiment loin.

> « L'amalgame sert la cause, ce faisant, tout est égal. On en revient à la même tendance de la société : tout est semblable, la vache industrielle et la vache paysanne, l'homme et l'animal, l'homme et la femme, il n'existe que de subtiles différences, tout est d'ordre culturel. […] Aujourd'hui il existe une distanciation absolue du corps, le corps est une idéologie, on peut se proclamer femme si on a un corps d'homme, on peut se proclamer moitié animal moitié homme, et dire, par exemple, que l'on peut rééduquer le loup pour qu'il ne tue pas le mouton. […] Il ne faut alors pas s'étonner d'entendre lors d'un dîner — cela peut arriver — "je me sens chat", ou zèbre, ou singe, sous prétexte que l'on est pareil, parce que fait des mêmes briques. »

Après avoir ravalé mon vomi, j'ai compris l'ampleur de son amalgame.
Il parle d'amalgame, mais c'est lui qui en fait entre le militantisme végan et le militantisme LGBTQI+.
Et en plus, il en profite pour se moquer, alors que ce n'est pas drôle du tout.
Selon lui, les causes LGBTQI+ et la cause animale sont un problème de société, pas une évolution possible, à laquelle il semble très fermé visiblement.

Et ça continue sur des arguments prétendument scientifiques, en voulant expliquer pourquoi les végans ne comprennent pas.
Pour lui, les végans pensent que tous les organismes sont égaux, parce que tous les organismes ont de l'ADN, cette molécule contenant l'information génétique.
Il veut démonter cet argument en disant que c'est con, on peut avoir 99,9 % d'ADN en commun et pourtant être différent.

Et il est encore plus incisif en disant :

> « Mais dire cela, c'est faire de la biologie. Or, comme le rappelle Frédéric Saumade _[le mec interviewé pour ce chapitre]_, elle n'existe pas pour beaucoup d'antispécistes, car, comme chez les puritains et les marxistes, la science est selon eux une superstructure d'écrasement blanche de toute sorte de différences. »

Encore un bel amalgame, saupoudré de condescendance, à mon goût.
Ce genre de mélange, ça me fait penser à une pub passée sur France Inter y'a [pas longtemps](https://mediateur.radiofrance.fr/non-classe/42-publicite-pour-le-confit-de-canard/).
L'auteur ne cherche pas à réfléchir, à aller dans la subtilité.
On mélange tout, on se plaint en disant que nous on a compris, et c'est tout.
Alors qu'on pourrait tenter de trouver des réponses plus subtiles à la question de ce qui nous différencie des autres animaux.
Dans la bienveillance et la compréhension de l'autre ?

<audio controls>
  <source src="{static}/media/2020/pub_confit.mp3" type="audio/mpeg">
	Your browser does not support the audio element.
</audio>
*Une horrible publicité entendue sur France Inter.*

---

Enfin, on arrive à sa conclusion. Il y écrit :

> « Chacun fait ce qu'il veut, dans une société libre. Mais dans le monde aux dimensions finies qui est le nôtre, chacun doit penser à ce qu'il fait. »

On l'a vu juste avant, l'auteur fait de gros amalgames, pour décridibiliser le mouvement végan.
Pourtant, il va globalement dans le même sens qu'une grosse partie des végan·e·s je pense : limiter la souffrance animale inutile, préserver l'environnement…
Qui est-il donc, lui qui essaie de mettre les gens les un·e·s contre les autres, avec par exemple le sous-titre très violent de son ouvrage *« Un nouvel intégrisme ? »* ?
Lui peut se permettre de juger tout le monde, mais les autres n'ont pas le droit de revendiquer leurs idées ?

Pour finir, la dernière phrase de son livre parle d'elle-même (désolé pour le divulgâchage) : _« En ce qui me concerne, je me serai réincarné en vache, et je me mangerai moi-même. »_.
Si ça c'est pas une négation de la biologie et de la mauvaise foi, je comprends pas.

## Conclusion

À mon tour de conclure.

J'ai trouvé qu'il y avait des chouettes trucs dans ce livre.
Beaucoup de domaines sont discutés : l'histoire du mouvement, les appropriations marketing et industrielles qui existent, les conséquences physiologiques de l'arrêt de la consommation de produits animaux… Bref, c'est complet.
Personnellement, j'ai été sensible à certains arguments, notamment ceux concernant l'élevage paysan et la viande _in vitro_ qui risque d'arriver à un moment ou à un autre.

Par contre, d'autres parties m'ont été désagréables à lire, bien souvent à cause de la forme.
Trop d'arguments utilisent des méthodes fallacieuses : les grosses généralités et la mauvaise foi en sont les principales.
En vrai, j'ai l'impression que l'auteur est un végan refoulé :

- il est plutôt d'accord pour manger moins de viande;
- il est contre les grosses fermes-usines avec des conditions déplorables pour les animaux;
- il est conscient des problèmes.

C'est seulement dans la manière de faire qu'il diffère avec les végan·e·s.
Là où lui ira chercher de la viande bio de bonne qualité, élevée dans une sorte de respect, eh bien les végan·e·s vont chercher autre chose encore.
C'est donc dommage de tenter de décridibiliser le mouvement, alors que beaucoup d'idées sont communes.

Pour comparer, je vous propose l'écoute de cette chouette émission des Pieds sur Terre : [Manger de la viande](https://www.franceculture.fr/emissions/les-pieds-sur-terre/manger-de-la-viande) (~30 minutes).
Les mêmes questionnements sont présents, mais ils sont traités d'une façon beaucoup plus sereine et calme.
Je trouve cette émission beaucoup plus constructive que le livre, car elle cherche moins à cliver.

Ça donne espoir. Ce genre de sujets est très clivant, sans trop que je sache pourquoi d'ailleurs.
Malgré tout, il semble possible de pouvoir l'évoquer sereinement, à l'image de cette émission de France Culture.
Continuons dans cette voie !

_#bienveillance_, merde !

---

**Mise à jour** : une [enquête sur la viande _in vitro_](https://www.franceinter.fr/emissions/secrets-d-info/secrets-d-info-14-novembre-2020) a été réalisée par France Inter.
Ils parlent des liens avec L214 : en gros, des financeurs "philanthropes" qui donnent de l'argent à la fois à L214 et à des startups qui travaillent sur la viande synthétique.
Bon, y'a pas de quoi s'affoler, ces dons à L214 représentent 7% du budget de l'asso.
