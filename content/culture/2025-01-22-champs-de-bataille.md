---
title: Lecture : Champs de bataille, l'histoire enfouie du remembrement 
date: 2025-01-22
category: Culture
tags: lecture, campagne, enquête, bretagne, ŒuvreNonMascu
---

C'était une des grosses sorties de la fin de l'année 2024.
Après *Algues vertes, l'histoire interdite*, Inès Léraud et Pierre van Hove reviennent avec un autre reportage dessiné, à propos du remembrement.

[![Couverture du livre]({static}/media/2025/champs-de-bataille.jpg){: .image-process-article-image}]({static}/media/2025/champs-de-bataille.jpg)
/// caption
Couverture du livre *Champs de bataille*.
///

### Les auteurices

Quelques mots sur l'enquêtrice pour commencer.
Inès Léraud est journaliste, elle fait de super reportages sur France Culture dans l'émission [les Pieds sur terre](https://www.radiofrance.fr/personnes/ines-leraud). 
Elle a notament réalisé deux séries d'une dizaine d'épisodes, intitulées "Journal breton", à propos de ce qu'elle découvre sur l'industrie agroalimentaire en Bretagne.
Pour ça, elle s'est installée dans le Kreiz Breizh, le centre Bretagne, et balade son micro un peu partout.

C'est dans le cadre de cette enquête qu'elle s'intéresse aux algues vertes. 
Des émissions lui sont consacrées, et puis elle décide d'en faire [une BD](https://www.editions-delcourt.fr/bd/series/serie-algues-vertes-l-histoire-interdite/album-algues-vertes-l-histoire-interdite), qui raconte son enquête. J'ai lu la version en langue bretonne et ai rédigé [un petit billet en breton](/lennet-em-eus-bezhin-glas-an-istor-difennet-br.html) à l'occasion d'ailleurs.
Cette histoire a même donné [un film](https://fr.wikipedia.org/wiki/Les_Algues_vertes).

Et donc, toujours installée dans le Kreiz Breizh, Inès Léraud a continué à enquêter, et s'est intéressée au remembrement en Bretagne.
C'est-à-dire à la réorganisation des parcelles pour faciliter l'accès des machines agricoles dans un objectif de rendement.

La BD des algues vertes, et celle sur le remembrement, ont été illustrées par Pierre van Hove, que je connais moins. Mais j'aime beaucoup son coup de crayon ! 
Réussir à transmettre autant d'information par le dessin est une prouesse.

Enfin, pour Champs de bataille, Inès Léraud a fait appel à un historien, Léandre Mandard.
C'est lui qui avait traduit en gallo la BD sur les algues vertes.
Il se trouve qu'il fait une thèse d'histoire sur… le remembrement.


### Résumé grossier, à coups de tractopelle

Je savais que c'était une histoire assez violente, notamment après avoir écouté [une émission des Pieds sur terre à ce sujet](https://www.radiofrance.fr/franceculture/podcasts/les-pieds-sur-terre/le-grand-remembrement-7583513) (devinez qui l'a produit ?).
Mais l'approche de la BD est un peu différente.
Il y a moins de témoignages et beaucoup plus d'archives : la télé de l'époque, les documents administratifs (comme les nombreuses demandes de *ne pas* remembrer, la plupart du temps rejetées sèchement).

La BD commence avec l'exemple des remembrements de Damgan, dans le Morbihan, puis de de Fégréac, en Loire-Atlantique.
On y voit la violence de l'événement : les paysan·e·s qui se demandent pourquoi on fait ça, que vont devenir les pommiers, fruits de plusieurs décennies de travail de greffe ?

La deuxième partie pose la question de l'origine de la loi qui a permis autant de violence et de dégâts.
Avec les archives on voit bien la condescendance de l'administration et des technocrates quand ils parlent de, ou s'adressent aux paysans et paysanes. 
Tout un système a été monté pour aller dans la direction de la *modernité* : de meilleurs salaires pour les agents administratifs quand il y a davantage de remembrement, l'usine automobile Citroën à côté de Rennes qui récupère la main d'œuvre, etc.

Une partie de la BD est consacrée à *comment c'était avant le remembrement*. Les fermes s'organisaient alors autour des animaux : quelques vaches par exemple, ou bien les chevaux qui avaient une place centrale. On apprend que les boucheries chevalines ont eu du travail lors du remembrement.

Le livre parle aussi des dégâts dans l'organisation de la société : il y avait les pro- et les anti-remembrement. Qui ne se parlaient plus.
À côté de ça, la lutte s'est aussi organisée. Il y a eu de nombreux festoù-noz de soutien, durant lesquels « *les meilleurs artistes de la région* » (c'est ce qui est écrit sur les belles affiches) chantaient, et notamment des textes écrits pour l'occasion.

Une grosse partie, triste, parle des conséquences écologiques, sur la biodiversité ou bien la régulation de l'eau. 
En fait, les connaissances scientifiques étaient déjà actuelles, quand la décision de remembrer a été prise. 
Ce n'était sûrement pas la préoccupation générale, mais des scientifiques alertaient sur les dangers à venir.
Et justement, certains agronomes croisés plus tôt dans l'ouvrage reviennent, pour dire qu'ils ont fait une grosse connerie, en promouvant le remembrement.

Enfin, le livre est riche d'annexes, de coupures de journaux, des textes des chants en français ou breton.

### Avis perso : c'est dense mais ça se dévore vite !

De mon côté, j'ai trouvé plein de références à des noms que je connais, de près ou de loin.
Des entreprises, comme l'Office central de Landerneau, qui a donné ensuite Coopagri (qui a donné ensuite l'actuel Eureden), très grosse coopérative agricole.
Le syndicat de la FNSEA est très présent aussi.

Dans les quelques témoignages qu'il y a, on ressent l'émotion des personnes qui ont vécu le remembrement. Ça a grave foutu la merde dans les campagnes, plus personne ne se parlait, en gros.

Si la BD est assez dense, car il y a beaucoup d'informations et beaucoup de sources mentionnées, elle se lit bien. 
C'est le pouvoir de la BD, bien pris en main par Pierre van Hove, le dessinateur.

C'est une BD utile pour comprendre l'état du paysage breton actuel. L'origine du système agroalimentaire breton aussi. 
Et ça peut donner des clés pour le futur : on sait au moins ce qu'il ne faut pas faire.

### Pour aller plus loin
Pour compléter, je viens de tomber sur une petite [série de vidéos documentaires de L214 à propos de la FNSEA](https://www.youtube.com/watch?v=NykYr5kngoY&list=PLaqPfBnj8_CVTy6MmamNRx47FkawaApiK). De nombreux noms reviennent : après avoir lu la BD, on n'est pas perdu.
