---
title: Conseil podcast : Comment finir une guerre, sur Arte Radio
date: 2025-01-16
category: Culture
tags: podcast, #ConseilPodcast, euskadi, guerre, paix, langue régionale, ŒuvreNonMascu
---

C'est une petite recommandation podcastique que je propose aujourd'hui.

Il s'agit d'une série de 8 épisodes sur Arte Radio : [Comment finir une guerre](https://www.arteradio.com/son/61676369/comment_finir_une_guerre_1_8), signée Myriam Prévost.

[![Capture d'écran de la page du premier épisode de Comment finir une guerre]({static}/media/2025/comment-finir-guerre.png){: .image-process-large-image }]({static}/media/2025/comment-finir-guerre.png)
/// caption
Capture d'écran de la page du premier épisode de Comment finir une guerre, sur Arte Radio.
///

Ça parle de l'histoire de l'organisation armée ETA (*Euskadi Ta Askatasuna*, Pays basque et Liberté) : d'où elle vient, pourquoi elle en est venue à s'armer.

Mais surtout, le gros sujet, c'est comment l'ETA a voulu se désarmer et entrer en procesus de paix.
Les membres de cette organisation ont fait des pieds et des mains pour tenter d'y arriver, mais les autres belligérants, les États français et espagnols, n'ont pas vraiment aidé. Loin de là.

En 2011 donc, l'ETA annonce la fin de la lutte armée, essaie de retrouver puis détruire les armes. C'est compliqué, le camp d'en face prend parfois ça pour des préparations d'attaques terroristes.
L'ETA attend de son côté de la table pour faire la paix, et les États continuent de traquer, arrêter, et parfois torturer les militants.

Ce podcast donne la parole aux acteurices du processus de paix, et c'est très très bien monté.

Le sujet me touche un peu, car il y quelques éléments communs avec ce qui s'est passé en Bretagne.
Notamment les actions de l'État pour détruire les régionales, en interdisant aux enfants de la parler à l'école, et en l'associant à la punition et surtout la honte.

Quelques passages sont très forts, durs même.
Je pense à l'épisode 5 par exemple, qui parle principalement de torture.

> « C'est une stratégie, la torture. C'est une stratégie qui vise à faire taire, elle ne vise pas à faire parler »
>
> *– Pauline Guelle, doctorante en droit public à l'Université de Pau et des Pays de l'Adour*

Je n'avais jamais véritablement entendu parler de torture. 
C'est un concept difficile à imaginer. C'est vague, c'est flou, je n'avais que peu de représentation.
(D'ailleurs, ça me fait penser au traitement médiatique de la mort de Jean-Marie Le Pen, qui élude complètement les tortures qu'il a pu commettre en Algérie[ref]Voir l'émission d'Arrêt sur Images consacrée au traitement médiatique de la mort de Le Pen : [https://www.arretsurimages.net/emissions/arret-sur-images/mort-de-jean-marie-le-pen-le-traitement-mediatique-banalise-sa-haine](https://www.arretsurimages.net/emissions/arret-sur-images/mort-de-jean-marie-le-pen-le-traitement-mediatique-banalise-sa-haine)[/ref].)
Là c'est… fort.
C'est concret, c'est dur, c'est à ne pas mettre dans toutes les oreilles mais c'est important quand même.


Et donc, toute cette petite série de 8 épisodes, je la recommande chaudement !
Et je pense que je vais publier d'autres recommandations d'écoute de balados qui m'ont marqué.

