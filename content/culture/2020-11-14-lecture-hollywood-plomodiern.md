---
title: J'ai lu : « Hollywood Plomodiern » de Stéphane Grangier
date: 2020-11-14
category: Culture
tags: lecture, bretagne, fiction
---

Je continue dans ma lancée de lecture de livres.
Aujourd'hui, je vous présente _Hollywood Plomodiern_ de Stéphane Grangier, un livre contant une histoire complètement barrée, en Bretagne.

<!--{{< figure src="./couverture.jpg" title="La couverture de « Hollywood Plomodiern »." class="center img-40" alt="La couverture de « Hollywood Plomodiern ».">}}-->

## Petit résumé (sans divulgâchage)

Un SDF est sous son arrêt de bus.
Tout à coup, il entend un grand fracas en bas de la route.
Il se lève et marche vers là d'où le bruit est parvenu.
Il aperçoit alors une Porsche rouge en plein milieu d'un champ, et à côté un Noir un peu choqué.
Il s'approche encore et voit une personne allongée par terre, visiblement inconsciente.
Le SDF tente alors de savoir si la personne est vivante, mais ne sent pas de pouls ni n'entend de respiration.
C'est alors qu'un bruit lointain survient : une voiture va très bientôt arriver.
Ni une ni deux, sans demander au Noir, le SDF prend le macabé et le jette dans le coffre.

S'ensuit un périple génial, à travers la Bretagne, pour essayer de se débarrasser du corps.
On a alors le loisir de découvrir les personnages : un SDF qui a abandonné tout espoir, et qui est également abandonné par la société ; et un joueur de football du Stade Rennais.

## Mon petit avis

J'ai tout de suite accroché à ce livre un peu barré.
La prose est très fluide, comme si l'histoire était racontée par un copain, juste à côté.
Bon, il est vrai que le langage est plutôt (très) cru.
D'habitude je n'aime pas trop ça, mais on s'y fait, et je trouve que ça apporte beaucoup au récit.
Attention tout de même, certains passages sont clairement interdits aux moins de 18 ans ([NSFW](https://fr.wikipedia.org/wiki/Not_safe_for_work) quoi).

Ensuite, un autre super point c'est que l'histoire se passe en Bretagne.
Une Bretagne connue par l'auteur visiblement, tant certains détails sont réalistes.
On passe de l'Ille-et-Vilaine (plus précisément le pays de Rennes) à la belle côte du Finistère, à Plomodiern.
Ça parle aussi de marques bien bretonnes, juste assez du monde du football (le côté réaliste), entre autres choses.
La réalité est vraiment pregnante, et ça rend vraiment bien.

De façon tout à fait complémentaire, il y a de très belles choses dans le livre.
Il y a notamment une chouette description d'un moment sous drogue, entre autres.
Ça part un peu dans tous les sens et c'est rigolo à lire.
Je trouve que ça en fait quelque chose de très poétique.

Pour conclure donc, j'ai dévoré ce livre avec grand plaisir !
