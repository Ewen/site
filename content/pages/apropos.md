Title: À propos
Date: 2020-01-01
Slug: a-propos

Bonjour et bienvenue sur ce petit bout d'internet, **fait à la main** avec du logiciel libre. Vous trouverez ici quelques informations sur moi-même ainsi que sur mes projets.

Bonne visite ! 🎉

### Qui suis-je ?

Je m'appelle Ewen et j'habite en Bretagne.
J'arrive à occuper mon temps entre :

- écouter des podcasts
- apprendre le trombone et faire des percussions dans le groupe [Hej da ru](https://linktr.ee/hejdaru)
- maintenir quelques [services numériques](https://fedi.bzh) en ligne
- traduire des logiciels libres en breton (comme [AntennaPod](https://antennapod.org) ou [Castopod](https://castopod.org))
- faire des podcasts quand je peux (publiés sur [podkast.fedi.bzh](https://podkast.fedi.bzh))

Je travaille actuellement au sein de la [plateforme de l'inclusion](https://inclusion.gouv.fr) en tant que développeur.

### Foire aux questions

#### Ce n'est pas ton premier site/blog. Pourquoi ?

Je crois que j'ai du mal à rester en place. Jusqu'à présent j'ai changé plus souvent de fonctionnement que rédigé d'articles, mais je vais essayer d'inverser la tendance.

#### C'est quoi ces noms de domaines, `corre.bio` et `korr.bzh` ?

Mon nom de famille, c'est Corre, un nom qu'on retrouve beaucoup en Basse-Bretagne.
Le `.bio`, c'est parce que j'ai commencé à jouer avec les noms de domaine quand j'étais en études de biologie.

Je voulais ardemment un nom de domaine en `.bzh`. Malheureusement, `corre.bzh` est déjà enregistré, alors j'ai enregistré `korr.bzh`, Korr étant l'orthographe en langue bretonne de mon patronyme.
