from datetime import datetime

AUTHOR = "Ewen Corre"
SITENAME = "Ewen Corre"
SITEURL = "http://127.0.0.1:8000"

PATH = "content"

TIMEZONE = "Europe/Paris"

DEFAULT_LANG = "fr"

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# This theme is heavily inspired by Nicolas Lœuillet's theme (CC0)
# https://github.com/nicosomb/journal
THEME = "themes/simple-theme"

# Social widget
SOCIAL = (
    ("Mastodon", "https://mastodon.fedi.bzh/@ewen"),
    ("Codeberg", "https://codeberg.org/ewen"),
    ("Github", "https://github.com/EwenKorr"),
    ("Podkastoù", "https://podkast.fedi.bzh"),
    ("Liberapay", "https://liberapay.com/ewen"),
)

STATIC_PATH = ("static",)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True

PLUGINS = [
    "pelican.plugins.simple_footnotes",
    "pelican.plugins.webring",
    "pelican.plugins.image_process",
    "pelican.plugins.tag_cloud",
]

# Webring
WEBRING_MAX_ARTICLES = 100
WEBRING_ARTICLES_PER_FEED = 1
TEMPLATE_PAGES = {"ailleurs.html": "pages/ailleurs.html"}
NOW = datetime.now()

# Images
IMAGE_PROCESS = {
    "article-image": ["scale_in 400 400 True"],
    "large-image": ["scale_in 900 900 False"],
    "thumb": ["crop 0 0 50% 50%", "scale_out 150 150 True", "crop 0 0 150 150"],
}
IMAGE_PROCESS_FORCE = True

# Markdown
MARKDOWN = {
    "extension_configs": {
        "markdown.extensions.codehilite": {"css_class": "highlight"},
        "markdown.extensions.extra": {},
        "markdown.extensions.meta": {},
        "pymdownx.tilde": {},
        "pymdownx.blocks.caption": {},
        "pymdownx.blocks.details": {},
    },
    "output_format": "html5",
}

# Tag cloud
TAG_CLOUD_STEPS = 5
TAG_CLOUD_MAX_ITEMS = 200
TAG_CLOUD_SORTING = "random"
TAG_CLOUD_BADGE = True
